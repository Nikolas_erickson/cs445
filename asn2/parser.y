%{
#include <cstdio>
#include <iostream>
#include <unistd.h>
#include "scanType.h"
using namespace std;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;

void yyerror(const char *msg);

void printToken(TokenData myData, string tokenName, int type = 0) {
   cout << "Line: " << myData.linenum << " Type: " << tokenName;
   if(type==0)
     cout << " Token: " << myData.tokenstr;
   if(type==1)
     cout << " Token: " << myData.nvalue;
   if(type==2)
     cout << " Token: " << myData.cvalue;
   cout << endl;
}

%}
%union
{
   struct   TokenData tinfo ;
}
%token   <tinfo>  OP
%token   <tinfo>  DEC INC
%token   <tinfo>  MIN MAX
%token   <tinfo>  MULASS DIVASS ADDASS SUBASS
%token   <tinfo>  NEQ GEQ LEQ EQ
%token   <tinfo>  AND BREAK BY DO ELSE
%token   <tinfo>  FOR IF NOT OR RETURN STATIC THEN TO WHILE
%token   <tinfo>  CHAR INT BOOL
%token   <tinfo>  PRECOMPILER
%token   <tinfo>  NUMCONST CHARCONST BOOLCONST STRINGCONST
%token   <tinfo>  ID
%token   <tinfo>  ERROR 
%type <tinfo>  term program
%%
program  :  program term
   |  term  {$$=$1;}
   ;
term  : 
      OP {printToken(yylval.tinfo, "OP");}
   |  DEC {printToken(yylval.tinfo, "DEC");}
   |  INC {printToken(yylval.tinfo, "INC");}
   |  MIN {printToken(yylval.tinfo, "MIN");}
   |  MAX {printToken(yylval.tinfo, "MAX");}
   |  MULASS {printToken(yylval.tinfo, "MULASS");}
   |  DIVASS {printToken(yylval.tinfo, "DIVASS");}
   |  ADDASS {printToken(yylval.tinfo, "ADDASS");}
   |  SUBASS {printToken(yylval.tinfo, "SUBASS");}
   |  NEQ {printToken(yylval.tinfo, "NEQ");}
   |  GEQ {printToken(yylval.tinfo, "GEQ");}
   |  LEQ {printToken(yylval.tinfo, "LEQ");}
   |  EQ {printToken(yylval.tinfo, "EQ");}
   |  AND {printToken(yylval.tinfo, "AND");}
   |  BREAK {printToken(yylval.tinfo, "BREAK");}
   |  BY {printToken(yylval.tinfo, "BY");}
   |  DO {printToken(yylval.tinfo, "DO");}
   |  ELSE {printToken(yylval.tinfo, "ELSE");}
   |  FOR {printToken(yylval.tinfo, "FOR");}
   |  IF {printToken(yylval.tinfo, "IF");}
   |  NOT {printToken(yylval.tinfo, "NOT");}
   |  OR {printToken(yylval.tinfo, "OR");}
   |  RETURN {printToken(yylval.tinfo, "RETURN");}
   |  STATIC {printToken(yylval.tinfo, "STATIC");}
   |  THEN {printToken(yylval.tinfo, "THEN");}
   |  TO {printToken(yylval.tinfo, "TO");}
   |  WHILE {printToken(yylval.tinfo, "WHILE");}
   |  CHAR {printToken(yylval.tinfo, "CHAR");}
   |  INT {printToken(yylval.tinfo, "INT");}
   |  BOOL {printToken(yylval.tinfo, "BOOL");}
   |  PRECOMPILER {printToken(yylval.tinfo, "PRECOMPILER");}
   |  ID {printToken(yylval.tinfo, "ID");}
   |  NUMCONST {printToken(yylval.tinfo, "NUMCONST");}
   |  CHARCONST {printToken(yylval.tinfo, "CHARCONST");}
   |  BOOLCONST {printToken(yylval.tinfo, "BOOLCONST");}
   |  STRINGCONST {printToken(yylval.tinfo, "STRINGCONST");}
   |  ERROR    {cout << "ERROR(SCANNER Line " << yylval.tinfo.linenum << "): Invalid input character " << yylval.tinfo.tokenstr << endl; }
   ;
%%

void yyerror (const char *msg)
{ 
   cout << "Error: " <<  msg << endl;
}

int main(int argc, char **argv) {
   yylval.tinfo.linenum = 1;
   int option, index;
   char *file = NULL;
   extern FILE *yyin;
   while ((option = getopt (argc, argv, "")) != -1)
      switch (option)
      {
      default:
         ;
      }
   if ( optind == argc ) yyparse();
   for (index = optind; index < argc; index++) 
   {
      yyin = fopen (argv[index], "r");
      yyparse();
      fclose (yyin);
   }
   return 0;
}

