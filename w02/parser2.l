/*
 *
 *
 *
 */


/// go through grammar, find keywords

";"		{return SEMI;}
"if"	{return IF;}
"then"	{return THEN;}
"else"	{return ELSE;}
"end"	{return END;}
"repeat"	{return REPEAT;}
"until"	{return UNTIL;}
"read"	{return READ;}
"write"	{return WRITE;}
":="	{return ASSIGN;}




/// regex for terminals

letter	[a-zA-Z]
digit	[0-9]
ID		{letter}[{letter}|{digit}]*
newline	\n
whitespace	[\t]+


%%