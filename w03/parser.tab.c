/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "parser.y"

#include <cstdio>
#include <iostream>
#include <unistd.h>

#include "scanType.h"
#include "treeNodes.h"
#include "treeUtils.h"


extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;

static TreeNode * syntaxTree; /* stores syntax tree for later return */

void yyerror(const char *msg);

using namespace std;

void printToken(TokenData myData, string tokenName, int type = 0) {
   cout << "Line: " << myData.linenum << " Type: " << tokenName;
   if(type==0)
     cout << " Token: " << myData.tokenstr;
   if(type==1)
     cout << " Token: " << myData.nvalue;
   if(type==2)
     cout << " Token: " << myData.cvalue;
   cout << endl;
}

TreeNode* makeRangeIntoFor(TokenData* mutId, TreeNode* rangeNode, TreeNode* stmtNode){
   TreeNode* forNode = newStmtNode(ForK, mutId);
   forNode->child[0] = newDeclNode(VarK, Integer, mutId);
   forNode->child[1] = rangeNode;
   forNode->child[2] = stmtNode;


   return forNode;
}

void printDebug(const char* st)
{
   if(1)
   {
      cout << st << endl;
   }
}


#line 122 "parser.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.tab.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_3_ = 3,                         /* '+'  */
  YYSYMBOL_4_ = 4,                         /* '-'  */
  YYSYMBOL_5_ = 5,                         /* '/'  */
  YYSYMBOL_6_ = 6,                         /* '*'  */
  YYSYMBOL_7_ = 7,                         /* '='  */
  YYSYMBOL_8_ = 8,                         /* '<'  */
  YYSYMBOL_9_ = 9,                         /* '>'  */
  YYSYMBOL_10_ = 10,                       /* '?'  */
  YYSYMBOL_11_ = 11,                       /* '%'  */
  YYSYMBOL_12_ = 12,                       /* '['  */
  YYSYMBOL_13_ = 13,                       /* ']'  */
  YYSYMBOL_14_ = 14,                       /* '{'  */
  YYSYMBOL_15_ = 15,                       /* '}'  */
  YYSYMBOL_16_ = 16,                       /* ','  */
  YYSYMBOL_17_ = 17,                       /* ';'  */
  YYSYMBOL_18_ = 18,                       /* ':'  */
  YYSYMBOL_19_ = 19,                       /* '('  */
  YYSYMBOL_20_ = 20,                       /* ')'  */
  YYSYMBOL_DEC = 21,                       /* DEC  */
  YYSYMBOL_INC = 22,                       /* INC  */
  YYSYMBOL_MIN = 23,                       /* MIN  */
  YYSYMBOL_MAX = 24,                       /* MAX  */
  YYSYMBOL_MULASS = 25,                    /* MULASS  */
  YYSYMBOL_DIVASS = 26,                    /* DIVASS  */
  YYSYMBOL_ADDASS = 27,                    /* ADDASS  */
  YYSYMBOL_SUBASS = 28,                    /* SUBASS  */
  YYSYMBOL_NEQ = 29,                       /* NEQ  */
  YYSYMBOL_GEQ = 30,                       /* GEQ  */
  YYSYMBOL_LEQ = 31,                       /* LEQ  */
  YYSYMBOL_EQ = 32,                        /* EQ  */
  YYSYMBOL_AND = 33,                       /* AND  */
  YYSYMBOL_OR = 34,                        /* OR  */
  YYSYMBOL_NOT = 35,                       /* NOT  */
  YYSYMBOL_BREAK = 36,                     /* BREAK  */
  YYSYMBOL_RETURN = 37,                    /* RETURN  */
  YYSYMBOL_STATIC = 38,                    /* STATIC  */
  YYSYMBOL_IF = 39,                        /* IF  */
  YYSYMBOL_THEN = 40,                      /* THEN  */
  YYSYMBOL_ELSE = 41,                      /* ELSE  */
  YYSYMBOL_FOR = 42,                       /* FOR  */
  YYSYMBOL_TO = 43,                        /* TO  */
  YYSYMBOL_BY = 44,                        /* BY  */
  YYSYMBOL_DO = 45,                        /* DO  */
  YYSYMBOL_WHILE = 46,                     /* WHILE  */
  YYSYMBOL_CHAR = 47,                      /* CHAR  */
  YYSYMBOL_INT = 48,                       /* INT  */
  YYSYMBOL_BOOL = 49,                      /* BOOL  */
  YYSYMBOL_PRECOMPILER = 50,               /* PRECOMPILER  */
  YYSYMBOL_NUMCONST = 51,                  /* NUMCONST  */
  YYSYMBOL_CHARCONST = 52,                 /* CHARCONST  */
  YYSYMBOL_BOOLCONST = 53,                 /* BOOLCONST  */
  YYSYMBOL_STRINGCONST = 54,               /* STRINGCONST  */
  YYSYMBOL_ID = 55,                        /* ID  */
  YYSYMBOL_ERROR = 56,                     /* ERROR  */
  YYSYMBOL_YYACCEPT = 57,                  /* $accept  */
  YYSYMBOL_program = 58,                   /* program  */
  YYSYMBOL_precomList = 59,                /* precomList  */
  YYSYMBOL_declList = 60,                  /* declList  */
  YYSYMBOL_decl = 61,                      /* decl  */
  YYSYMBOL_varDecl = 62,                   /* varDecl  */
  YYSYMBOL_scopedVarDecl = 63,             /* scopedVarDecl  */
  YYSYMBOL_varDeclList = 64,               /* varDeclList  */
  YYSYMBOL_varDeclInit = 65,               /* varDeclInit  */
  YYSYMBOL_varDeclId = 66,                 /* varDeclId  */
  YYSYMBOL_typeSpec = 67,                  /* typeSpec  */
  YYSYMBOL_funDecl = 68,                   /* funDecl  */
  YYSYMBOL_parms = 69,                     /* parms  */
  YYSYMBOL_parmList = 70,                  /* parmList  */
  YYSYMBOL_parmTypeList = 71,              /* parmTypeList  */
  YYSYMBOL_parmIdList = 72,                /* parmIdList  */
  YYSYMBOL_parmId = 73,                    /* parmId  */
  YYSYMBOL_stmt = 74,                      /* stmt  */
  YYSYMBOL_matched = 75,                   /* matched  */
  YYSYMBOL_iterRange = 76,                 /* iterRange  */
  YYSYMBOL_unmatched = 77,                 /* unmatched  */
  YYSYMBOL_expStmt = 78,                   /* expStmt  */
  YYSYMBOL_compoundStmt = 79,              /* compoundStmt  */
  YYSYMBOL_localDecls = 80,                /* localDecls  */
  YYSYMBOL_stmtList = 81,                  /* stmtList  */
  YYSYMBOL_returnStmt = 82,                /* returnStmt  */
  YYSYMBOL_breakStmt = 83,                 /* breakStmt  */
  YYSYMBOL_exp = 84,                       /* exp  */
  YYSYMBOL_assignop = 85,                  /* assignop  */
  YYSYMBOL_simpleExp = 86,                 /* simpleExp  */
  YYSYMBOL_andExp = 87,                    /* andExp  */
  YYSYMBOL_unaryRelExp = 88,               /* unaryRelExp  */
  YYSYMBOL_relExp = 89,                    /* relExp  */
  YYSYMBOL_relop = 90,                     /* relop  */
  YYSYMBOL_minmaxExp = 91,                 /* minmaxExp  */
  YYSYMBOL_minmaxop = 92,                  /* minmaxop  */
  YYSYMBOL_sumExp = 93,                    /* sumExp  */
  YYSYMBOL_sumop = 94,                     /* sumop  */
  YYSYMBOL_mulExp = 95,                    /* mulExp  */
  YYSYMBOL_mulop = 96,                     /* mulop  */
  YYSYMBOL_unaryExp = 97,                  /* unaryExp  */
  YYSYMBOL_unaryop = 98,                   /* unaryop  */
  YYSYMBOL_factor = 99,                    /* factor  */
  YYSYMBOL_mutable = 100,                  /* mutable  */
  YYSYMBOL_immutable = 101,                /* immutable  */
  YYSYMBOL_call = 102,                     /* call  */
  YYSYMBOL_args = 103,                     /* args  */
  YYSYMBOL_argList = 104,                  /* argList  */
  YYSYMBOL_constant = 105                  /* constant  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  14
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   224

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  57
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  49
/* YYNRULES -- Number of rules.  */
#define YYNRULES  114
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  176

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   293


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    11,     2,     2,
      19,    20,     6,     3,    16,     4,     2,     5,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    18,    17,
       8,     7,     9,    10,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    12,     2,    13,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    14,     2,    15,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,    83,    83,    87,    93,    98,   105,   123,   127,   129,
     133,   144,   155,   166,   184,   189,   191,   194,   196,   203,
     205,   207,   211,   215,   220,   223,   225,   242,   245,   257,
     274,   277,   279,   285,   287,   291,   293,   295,   299,   300,
     302,   303,   305,   307,   311,   313,   315,   317,   322,   324,
     327,   332,   350,   353,   371,   374,   376,   379,   382,   387,
     392,   397,   399,   400,   401,   402,   403,   405,   410,   412,
     417,   419,   424,   426,   431,   433,   434,   435,   436,   437,
     438,   440,   445,   448,   449,   451,   456,   458,   459,   462,
     467,   470,   471,   472,   475,   490,   493,   494,   495,   498,
     499,   502,   507,   514,   516,   517,   519,   525,   527,   529,
     546,   548,   556,   562,   569
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "'+'", "'-'", "'/'",
  "'*'", "'='", "'<'", "'>'", "'?'", "'%'", "'['", "']'", "'{'", "'}'",
  "','", "';'", "':'", "'('", "')'", "DEC", "INC", "MIN", "MAX", "MULASS",
  "DIVASS", "ADDASS", "SUBASS", "NEQ", "GEQ", "LEQ", "EQ", "AND", "OR",
  "NOT", "BREAK", "RETURN", "STATIC", "IF", "THEN", "ELSE", "FOR", "TO",
  "BY", "DO", "WHILE", "CHAR", "INT", "BOOL", "PRECOMPILER", "NUMCONST",
  "CHARCONST", "BOOLCONST", "STRINGCONST", "ID", "ERROR", "$accept",
  "program", "precomList", "declList", "decl", "varDecl", "scopedVarDecl",
  "varDeclList", "varDeclInit", "varDeclId", "typeSpec", "funDecl",
  "parms", "parmList", "parmTypeList", "parmIdList", "parmId", "stmt",
  "matched", "iterRange", "unmatched", "expStmt", "compoundStmt",
  "localDecls", "stmtList", "returnStmt", "breakStmt", "exp", "assignop",
  "simpleExp", "andExp", "unaryRelExp", "relExp", "relop", "minmaxExp",
  "minmaxop", "sumExp", "sumop", "mulExp", "mulop", "unaryExp", "unaryop",
  "factor", "mutable", "immutable", "call", "args", "argList", "constant", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-140)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     134,  -140,  -140,  -140,  -140,   -14,    17,   144,    25,  -140,
    -140,   -34,  -140,    48,  -140,  -140,    25,  -140,    32,    69,
    -140,    15,    -5,    38,    52,  -140,    30,    48,    45,  -140,
     155,    94,    92,  -140,   103,    48,   102,    99,   114,  -140,
    -140,  -140,  -140,   155,   155,  -140,  -140,  -140,  -140,    35,
      97,    95,  -140,  -140,    59,    89,   119,  -140,   169,  -140,
    -140,  -140,  -140,  -140,   122,    -5,  -140,  -140,   124,   117,
     155,    88,   155,  -140,  -140,  -140,  -140,  -140,  -140,  -140,
     131,    97,    77,  -140,  -140,   103,   130,  -140,   155,   155,
     155,   155,  -140,  -140,  -140,  -140,  -140,  -140,  -140,  -140,
     169,   169,  -140,  -140,   169,  -140,  -140,  -140,   169,  -140,
    -140,  -140,   -29,  -140,  -140,   136,    31,   153,   -22,  -140,
    -140,  -140,  -140,  -140,  -140,  -140,  -140,   155,  -140,  -140,
     138,  -140,   156,   148,    95,  -140,   123,    89,   119,  -140,
      48,  -140,    45,    24,  -140,   103,   155,   103,  -140,  -140,
    -140,   155,    45,   146,  -140,  -140,  -140,   137,   132,     3,
    -140,  -140,  -140,   150,  -140,   103,   103,   155,  -140,  -140,
    -140,  -140,  -140,   -19,   155,    97
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,    21,    19,    20,     5,     0,     0,     0,     3,     7,
       8,     0,     9,    25,     1,     4,     2,     6,    17,     0,
      14,    15,     0,     0,    24,    27,     0,    25,     0,    10,
       0,    31,    28,    30,     0,     0,     0,     0,    17,    13,
      96,    97,    98,     0,     0,   111,   112,   114,   113,   101,
      16,    68,    70,    72,    74,    82,    86,    90,     0,    95,
     100,    99,   104,   105,     0,     0,    52,    49,     0,     0,
       0,     0,     0,    23,    33,    34,    38,    39,    40,    41,
       0,    61,   100,    26,    18,     0,     0,    71,     0,   108,
       0,     0,    76,    77,    84,    83,    80,    78,    75,    79,
       0,     0,    87,    88,     0,    92,    91,    93,     0,    94,
      32,    29,    54,    57,    55,     0,     0,     0,     0,    48,
      62,    60,    59,    65,    66,    63,    64,     0,    22,   103,
       0,   110,     0,   107,    67,    69,    73,    81,    85,    89,
       0,    51,     0,     0,    56,     0,     0,     0,    58,   102,
     106,     0,     0,     0,    50,    53,    44,    33,     0,     0,
      36,    46,   109,     0,    12,     0,     0,     0,    11,    35,
      45,    37,    47,    42,     0,    43
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -140,  -140,  -140,   173,     6,  -140,  -140,  -107,   157,  -140,
     -11,  -140,   159,  -140,   152,  -140,   133,   -81,  -134,  -140,
    -139,  -140,  -140,  -140,  -140,  -140,  -140,   -40,  -140,   -30,
     105,   -38,  -140,  -140,    96,  -140,   100,  -140,    93,  -140,
     -51,  -140,  -140,   -33,  -140,  -140,  -140,  -140,  -140
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,     6,     7,     8,     9,    10,   141,    19,    20,    21,
      11,    12,    23,    24,    25,    32,    33,    73,    74,   158,
      75,    76,    77,   112,   143,    78,    79,    80,   127,    81,
      51,    52,    53,   100,    54,   101,    55,   104,    56,   108,
      57,    58,    59,    60,    61,    62,   132,   133,    63
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      50,    82,    22,    86,   128,    13,    87,   109,   161,   140,
      82,   157,    90,   160,    17,    90,    22,    14,     1,     2,
       3,    18,    17,   147,    22,   174,   170,   172,    40,   115,
      41,   169,   171,    30,    42,   153,    82,    90,    66,   154,
     116,    67,   118,    43,    26,   163,   167,    88,   130,   131,
      31,    27,    82,   135,    89,    82,    82,   139,    34,    44,
      68,    69,   155,    70,   156,    90,    71,    92,    93,    35,
      72,   145,     1,     2,     3,    45,    46,    47,    48,    49,
       5,    36,    94,    95,   120,    28,    29,   148,    96,    97,
      98,    99,   102,   103,    82,     1,     2,     3,   121,   122,
      38,   142,   123,   124,   125,   126,    64,    40,    65,    41,
      82,   162,    82,    42,    82,    84,   159,    66,    82,    85,
      67,    40,    43,    41,   105,   106,    26,    42,    91,   152,
     107,    90,    82,    82,   114,   110,    43,   173,    44,    68,
      69,   113,    70,   117,   175,    71,    94,    95,   119,    72,
     129,   149,    44,   144,    45,    46,    47,    48,    49,    40,
     146,    41,    28,   164,   151,    42,    28,   168,    45,    46,
      47,    48,    49,    40,    43,    41,   150,   166,   165,    42,
      16,     1,     2,     3,     4,    39,    37,    83,    43,     5,
      44,     1,     2,     3,    15,   134,   136,   138,   111,     5,
       0,   137,     0,     0,     0,     0,    45,    46,    47,    48,
      49,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      45,    46,    47,    48,    49
};

static const yytype_int16 yycheck[] =
{
      30,    34,    13,    43,    85,    19,    44,    58,   147,    38,
      43,   145,    34,   147,     8,    34,    27,     0,    47,    48,
      49,    55,    16,    45,    35,    44,   165,   166,     4,    69,
       6,   165,   166,    18,    10,   142,    69,    34,    14,    15,
      70,    17,    72,    19,    12,   152,    43,    12,    88,    89,
      55,    19,    85,    91,    19,    88,    89,   108,    20,    35,
      36,    37,   143,    39,   145,    34,    42,     8,     9,    17,
      46,    40,    47,    48,    49,    51,    52,    53,    54,    55,
      55,    51,    23,    24,     7,    16,    17,   127,    29,    30,
      31,    32,     3,     4,   127,    47,    48,    49,    21,    22,
      55,   112,    25,    26,    27,    28,    12,     4,    16,     6,
     143,   151,   145,    10,   147,    13,   146,    14,   151,    20,
      17,     4,    19,     6,     5,     6,    12,    10,    33,   140,
      11,    34,   165,   166,    17,    13,    19,   167,    35,    36,
      37,    17,    39,    55,   174,    42,    23,    24,    17,    46,
      20,    13,    35,    17,    51,    52,    53,    54,    55,     4,
       7,     6,    16,    17,    16,    10,    16,    17,    51,    52,
      53,    54,    55,     4,    19,     6,    20,    45,    41,    10,
       7,    47,    48,    49,    50,    28,    27,    35,    19,    55,
      35,    47,    48,    49,    50,    90,   100,   104,    65,    55,
      -1,   101,    -1,    -1,    -1,    -1,    51,    52,    53,    54,
      55,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      51,    52,    53,    54,    55
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    47,    48,    49,    50,    55,    58,    59,    60,    61,
      62,    67,    68,    19,     0,    50,    60,    61,    55,    64,
      65,    66,    67,    69,    70,    71,    12,    19,    16,    17,
      18,    55,    72,    73,    20,    17,    51,    69,    55,    65,
       4,     6,    10,    19,    35,    51,    52,    53,    54,    55,
      86,    87,    88,    89,    91,    93,    95,    97,    98,    99,
     100,   101,   102,   105,    12,    16,    14,    17,    36,    37,
      39,    42,    46,    74,    75,    77,    78,    79,    82,    83,
      84,    86,   100,    71,    13,    20,    84,    88,    12,    19,
      34,    33,     8,     9,    23,    24,    29,    30,    31,    32,
      90,    92,     3,     4,    94,     5,     6,    11,    96,    97,
      13,    73,    80,    17,    17,    84,    86,    55,    86,    17,
       7,    21,    22,    25,    26,    27,    28,    85,    74,    20,
      84,    84,   103,   104,    87,    88,    91,    93,    95,    97,
      38,    63,    67,    81,    17,    40,     7,    45,    84,    13,
      20,    16,    67,    64,    15,    74,    74,    75,    76,    86,
      75,    77,    84,    64,    17,    41,    45,    43,    17,    75,
      77,    75,    77,    86,    44,    86
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    57,    58,    58,    59,    59,    60,    60,    61,    61,
      62,    63,    63,    64,    64,    65,    65,    66,    66,    67,
      67,    67,    68,    68,    69,    69,    70,    70,    71,    72,
      72,    73,    73,    74,    74,    75,    75,    75,    75,    75,
      75,    75,    76,    76,    77,    77,    77,    77,    78,    78,
      79,    80,    80,    81,    81,    82,    82,    83,    84,    84,
      84,    84,    85,    85,    85,    85,    85,    86,    86,    87,
      87,    88,    88,    89,    89,    90,    90,    90,    90,    90,
      90,    91,    91,    92,    92,    93,    93,    94,    94,    95,
      95,    96,    96,    96,    97,    97,    98,    98,    98,    99,
      99,   100,   100,   101,   101,   101,   102,   103,   103,   104,
     104,   105,   105,   105,   105
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     1,     2,     1,     2,     1,     1,     1,
       3,     4,     3,     3,     1,     1,     3,     1,     4,     1,
       1,     1,     6,     5,     1,     0,     3,     1,     2,     3,
       1,     1,     3,     1,     1,     6,     4,     6,     1,     1,
       1,     1,     3,     5,     4,     6,     4,     6,     2,     1,
       4,     2,     0,     2,     0,     2,     3,     2,     3,     2,
       2,     1,     1,     1,     1,     1,     1,     3,     1,     3,
       1,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     3,     1,     1,     1,     3,     1,     1,     1,     3,
       1,     1,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     4,     3,     1,     1,     4,     1,     0,     3,
       1,     1,     1,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* program: precomList declList  */
#line 84 "parser.y"
                        {
                           (yyval.tree) = syntaxTree = (yyvsp[0].tree);
                        }
#line 1355 "parser.tab.c"
    break;

  case 3: /* program: declList  */
#line 88 "parser.y"
                        {
                           (yyval.tree) = syntaxTree = (yyvsp[0].tree);
                        }
#line 1363 "parser.tab.c"
    break;

  case 4: /* precomList: precomList PRECOMPILER  */
#line 94 "parser.y"
                        {
                           cout << yylval.tinfo->tokenstr << endl; 
                           (yyval.tree) = NULL;
                        }
#line 1372 "parser.tab.c"
    break;

  case 5: /* precomList: PRECOMPILER  */
#line 99 "parser.y"
                        {
                           cout << yylval.tinfo->tokenstr << endl;
                           (yyval.tree) = NULL;
                        }
#line 1381 "parser.tab.c"
    break;

  case 6: /* declList: declList decl  */
#line 106 "parser.y"
                        {
                           if((yyvsp[-1].tree) != NULL)
                           {
                              // addSibling
                              TreeNode * t = (yyvsp[-1].tree);
                              while(t->sibling != NULL)
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-1].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);
                           }
                        }
#line 1403 "parser.tab.c"
    break;

  case 7: /* declList: decl  */
#line 124 "parser.y"
                        {  (yyval.tree) = (yyvsp[0].tree); }
#line 1409 "parser.tab.c"
    break;

  case 8: /* decl: varDecl  */
#line 128 "parser.y"
                        {  (yyval.tree) = (yyvsp[0].tree);}
#line 1415 "parser.tab.c"
    break;

  case 9: /* decl: funDecl  */
#line 130 "parser.y"
                        {  (yyval.tree) = (yyvsp[0].tree);}
#line 1421 "parser.tab.c"
    break;

  case 10: /* varDecl: typeSpec varDeclList ';'  */
#line 134 "parser.y"
                        {
                           TreeNode* t = (yyvsp[-1].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-2].expType);
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[-1].tree);
                        }
#line 1435 "parser.tab.c"
    break;

  case 11: /* scopedVarDecl: STATIC typeSpec varDeclList ';'  */
#line 145 "parser.y"
                        {
                           TreeNode* t = (yyvsp[-1].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-2].expType);           // set to typeSpec
                              t->isStatic = true;     // flag as static
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[-1].tree);
                        }
#line 1450 "parser.tab.c"
    break;

  case 12: /* scopedVarDecl: typeSpec varDeclList ';'  */
#line 156 "parser.y"
                        {
                           TreeNode* t = (yyvsp[-1].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-2].expType);
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[-1].tree);
                        }
#line 1464 "parser.tab.c"
    break;

  case 13: /* varDeclList: varDeclList ',' varDeclInit  */
#line 167 "parser.y"
                        {
                           if((yyvsp[-2].tree) != NULL)
                           {
                              TreeNode * t = (yyvsp[-2].tree);
                              while(t->sibling != NULL)
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-2].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);

                           }
                        }
#line 1486 "parser.tab.c"
    break;

  case 14: /* varDeclList: varDeclInit  */
#line 185 "parser.y"
                        {
                           (yyval.tree) = (yyvsp[0].tree);
                        }
#line 1494 "parser.tab.c"
    break;

  case 15: /* varDeclInit: varDeclId  */
#line 190 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 1500 "parser.tab.c"
    break;

  case 16: /* varDeclInit: varDeclId ':' simpleExp  */
#line 192 "parser.y"
                        { (yyval.tree) = (yyvsp[-2].tree); if ((yyval.tree) != NULL) (yyval.tree)->child[0] = (yyvsp[0].tree); }
#line 1506 "parser.tab.c"
    break;

  case 17: /* varDeclId: ID  */
#line 195 "parser.y"
                        { (yyval.tree) = newDeclNode(VarK, UndefinedType, (yyvsp[0].tinfo)); }
#line 1512 "parser.tab.c"
    break;

  case 18: /* varDeclId: ID '[' NUMCONST ']'  */
#line 197 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(VarK, UndefinedType, (yyvsp[-3].tinfo));
                           (yyval.tree)->isArray = true;
                           (yyval.tree)->size = (yyvsp[-1].tinfo)->nvalue;
                        }
#line 1522 "parser.tab.c"
    break;

  case 19: /* typeSpec: INT  */
#line 204 "parser.y"
                        { (yyval.expType) = Integer; }
#line 1528 "parser.tab.c"
    break;

  case 20: /* typeSpec: BOOL  */
#line 206 "parser.y"
                        { (yyval.expType) = Boolean; }
#line 1534 "parser.tab.c"
    break;

  case 21: /* typeSpec: CHAR  */
#line 208 "parser.y"
                        { (yyval.expType) = Char; }
#line 1540 "parser.tab.c"
    break;

  case 22: /* funDecl: typeSpec ID '(' parms ')' stmt  */
#line 212 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(FuncK, (yyvsp[-5].expType), (yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 1548 "parser.tab.c"
    break;

  case 23: /* funDecl: ID '(' parms ')' stmt  */
#line 216 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(FuncK, UndefinedType, (yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 1556 "parser.tab.c"
    break;

  case 24: /* parms: parmList  */
#line 221 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 1562 "parser.tab.c"
    break;

  case 25: /* parms: %empty  */
#line 223 "parser.y"
                        { (yyval.tree) = NULL;}
#line 1568 "parser.tab.c"
    break;

  case 26: /* parmList: parmList ';' parmTypeList  */
#line 226 "parser.y"
                        {
                           if((yyvsp[-2].tree) != NULL)
                           {
                              TreeNode * t = (yyvsp[-2].tree);
                              while( t->sibling != NULL )
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-2].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);
                           }
                        }
#line 1589 "parser.tab.c"
    break;

  case 27: /* parmList: parmTypeList  */
#line 243 "parser.y"
                        { (yyval.tree)  = (yyvsp[0].tree); }
#line 1595 "parser.tab.c"
    break;

  case 28: /* parmTypeList: typeSpec parmIdList  */
#line 246 "parser.y"
                        {
                           TreeNode* t = (yyvsp[0].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-1].expType);           // set to typeSpec
                              t->isStatic = true;     // flag as static
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[0].tree);
                        }
#line 1610 "parser.tab.c"
    break;

  case 29: /* parmIdList: parmIdList ',' parmId  */
#line 258 "parser.y"
                        {
                           if((yyvsp[-2].tree) != NULL)
                           {
                              TreeNode * t = (yyvsp[-2].tree);
                              while( t->sibling != NULL )
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-2].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);
                           }
                        }
#line 1631 "parser.tab.c"
    break;

  case 30: /* parmIdList: parmId  */
#line 275 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 1637 "parser.tab.c"
    break;

  case 31: /* parmId: ID  */
#line 278 "parser.y"
                        { (yyval.tree) = newDeclNode(ParamK, UndefinedType, (yyvsp[0].tinfo)); }
#line 1643 "parser.tab.c"
    break;

  case 32: /* parmId: ID '[' ']'  */
#line 280 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(ParamK, UndefinedType, (yyvsp[-2].tinfo));
                           (yyval.tree)->isArray = true;
                        }
#line 1652 "parser.tab.c"
    break;

  case 33: /* stmt: matched  */
#line 286 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree);}
#line 1658 "parser.tab.c"
    break;

  case 34: /* stmt: unmatched  */
#line 288 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 1664 "parser.tab.c"
    break;

  case 35: /* matched: IF simpleExp THEN matched ELSE matched  */
#line 292 "parser.y"
                        { (yyval.tree) = newStmtNode(IfK, (yyvsp[-5].tinfo), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1670 "parser.tab.c"
    break;

  case 36: /* matched: WHILE simpleExp DO matched  */
#line 294 "parser.y"
                        { (yyval.tree) = newStmtNode(WhileK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1676 "parser.tab.c"
    break;

  case 37: /* matched: FOR ID '=' iterRange DO matched  */
#line 296 "parser.y"
                        {
                           (yyval.tree) = makeRangeIntoFor((yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 1684 "parser.tab.c"
    break;

  case 39: /* matched: compoundStmt  */
#line 301 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 1690 "parser.tab.c"
    break;

  case 42: /* iterRange: simpleExp TO simpleExp  */
#line 306 "parser.y"
                        { (yyval.tree) = newStmtNode(RangeK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1696 "parser.tab.c"
    break;

  case 43: /* iterRange: simpleExp TO simpleExp BY simpleExp  */
#line 308 "parser.y"
                        { (yyval.tree) = newStmtNode(RangeK, (yyvsp[-3].tinfo), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1702 "parser.tab.c"
    break;

  case 44: /* unmatched: IF simpleExp THEN stmt  */
#line 312 "parser.y"
                        { (yyval.tree) = newStmtNode(IfK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1708 "parser.tab.c"
    break;

  case 45: /* unmatched: IF simpleExp THEN matched ELSE unmatched  */
#line 314 "parser.y"
                        { (yyval.tree) = newStmtNode(IfK, (yyvsp[-5].tinfo), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1714 "parser.tab.c"
    break;

  case 46: /* unmatched: WHILE simpleExp DO unmatched  */
#line 316 "parser.y"
                        { (yyval.tree) = newStmtNode(WhileK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 1720 "parser.tab.c"
    break;

  case 47: /* unmatched: FOR ID '=' iterRange DO unmatched  */
#line 318 "parser.y"
                        {
                           (yyval.tree) = makeRangeIntoFor((yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 1728 "parser.tab.c"
    break;

  case 48: /* expStmt: exp ';'  */
#line 323 "parser.y"
                        { (yyval.tree) = (yyvsp[-1].tree); }
#line 1734 "parser.tab.c"
    break;

  case 49: /* expStmt: ';'  */
#line 325 "parser.y"
                        { (yyval.tree) = NULL; }
#line 1740 "parser.tab.c"
    break;

  case 50: /* compoundStmt: '{' localDecls stmtList '}'  */
#line 328 "parser.y"
                        {
                           (yyval.tree) = newStmtNode(CompoundK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[-1].tree));
                        }
#line 1748 "parser.tab.c"
    break;

  case 51: /* localDecls: localDecls scopedVarDecl  */
#line 333 "parser.y"
                        {
                           if((yyvsp[-1].tree) != NULL)
                           {
                              TreeNode * t = (yyvsp[-1].tree);
                              while( t->sibling != NULL )
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-1].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);
                           }
                        }
#line 1769 "parser.tab.c"
    break;

  case 52: /* localDecls: %empty  */
#line 350 "parser.y"
                        { (yyval.tree) = NULL; }
#line 1775 "parser.tab.c"
    break;

  case 53: /* stmtList: stmtList stmt  */
#line 354 "parser.y"
                        {
                           if((yyvsp[-1].tree) != NULL)
                           {
                              TreeNode * t = (yyvsp[-1].tree);
                              while( t->sibling != NULL )
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-1].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);
                           }
                        }
#line 1796 "parser.tab.c"
    break;

  case 54: /* stmtList: %empty  */
#line 371 "parser.y"
                        { (yyval.tree) = NULL; }
#line 1802 "parser.tab.c"
    break;

  case 55: /* returnStmt: RETURN ';'  */
#line 375 "parser.y"
                        { (yyval.tree) = newStmtNode(ReturnK, (yyvsp[-1].tinfo)); }
#line 1808 "parser.tab.c"
    break;

  case 56: /* returnStmt: RETURN exp ';'  */
#line 377 "parser.y"
                        { (yyval.tree) = newStmtNode(ReturnK, (yyvsp[-2].tinfo), (yyvsp[-1].tree)); }
#line 1814 "parser.tab.c"
    break;

  case 57: /* breakStmt: BREAK ';'  */
#line 380 "parser.y"
                        { (yyval.tree) = newStmtNode(BreakK, (yyvsp[-1].tinfo)); }
#line 1820 "parser.tab.c"
    break;

  case 58: /* exp: mutable assignop exp  */
#line 383 "parser.y"
                        {
                           (yyval.tree) = newExpNode(AssignK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1829 "parser.tab.c"
    break;

  case 59: /* exp: mutable INC  */
#line 388 "parser.y"
                        {
                           (yyval.tree) = newExpNode(AssignK, (yyvsp[0].tinfo), (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 1838 "parser.tab.c"
    break;

  case 60: /* exp: mutable DEC  */
#line 393 "parser.y"
                        {
                           (yyval.tree) = newExpNode(AssignK, (yyvsp[0].tinfo), (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 1847 "parser.tab.c"
    break;

  case 67: /* simpleExp: simpleExp OR andExp  */
#line 406 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1856 "parser.tab.c"
    break;

  case 69: /* andExp: andExp AND unaryRelExp  */
#line 413 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1865 "parser.tab.c"
    break;

  case 71: /* unaryRelExp: NOT unaryRelExp  */
#line 420 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1874 "parser.tab.c"
    break;

  case 73: /* relExp: minmaxExp relop minmaxExp  */
#line 427 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1883 "parser.tab.c"
    break;

  case 81: /* minmaxExp: minmaxExp minmaxop sumExp  */
#line 441 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1892 "parser.tab.c"
    break;

  case 85: /* sumExp: sumExp sumop mulExp  */
#line 452 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1901 "parser.tab.c"
    break;

  case 89: /* mulExp: mulExp mulop unaryExp  */
#line 463 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 1910 "parser.tab.c"
    break;

  case 94: /* unaryExp: unaryop unaryExp  */
#line 476 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[0].tree));
                           if((yyval.tree)->attr.string[0] == '*')
                           {
                              free((yyval.tree)->attr.string);
                              (yyval.tree)->attr.string = strdup("sizeof");
                           }
                           else if((yyval.tree)->attr.string[0] == '-')
                           {
                              free((yyval.tree)->attr.string);
                              (yyval.tree)->attr.string = strdup("chsign");
                           }
                           // TODO - do the thing
                        }
#line 1929 "parser.tab.c"
    break;

  case 101: /* mutable: ID  */
#line 503 "parser.y"
                        {
                           (yyval.tree) = newExpNode(IdK, (yyvsp[0].tinfo));
                           // TODO - do the thing
                        }
#line 1938 "parser.tab.c"
    break;

  case 102: /* mutable: ID '[' exp ']'  */
#line 508 "parser.y"
                        {
                           TreeNode * var = newExpNode(IdK, (yyvsp[-3].tinfo));
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-2].tinfo), var, (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 1948 "parser.tab.c"
    break;

  case 103: /* immutable: '(' exp ')'  */
#line 515 "parser.y"
                        { (yyval.tree) = (yyvsp[-1].tree); }
#line 1954 "parser.tab.c"
    break;

  case 106: /* call: ID '(' args ')'  */
#line 520 "parser.y"
                        {
                           (yyval.tree) = newExpNode(CallK, (yyvsp[-3].tinfo), (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 1963 "parser.tab.c"
    break;

  case 108: /* args: %empty  */
#line 527 "parser.y"
                        { (yyval.tree) = NULL; }
#line 1969 "parser.tab.c"
    break;

  case 109: /* argList: argList ',' exp  */
#line 530 "parser.y"
                        {
                           if((yyvsp[-2].tree) != NULL)
                           {
                              TreeNode * t = (yyvsp[-2].tree);
                              while( t->sibling != NULL )
                              {
                                 t = t->sibling;
                              }
                              t->sibling = (yyvsp[0].tree);
                              (yyval.tree) = (yyvsp[-2].tree);
                           }
                           else
                           {
                              (yyval.tree) = (yyvsp[0].tree);
                           }
                        }
#line 1990 "parser.tab.c"
    break;

  case 111: /* constant: NUMCONST  */
#line 549 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Integer;
                           (yyval.tree)->attr.value = (yyvsp[0].tinfo)->nvalue;
                           (yyval.tree)->lineno = (yyvsp[0].tinfo)->linenum;
                           // TODO - do the thing
                        }
#line 2002 "parser.tab.c"
    break;

  case 112: /* constant: CHARCONST  */
#line 557 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Char;
                           (yyval.tree)->attr.value = (yyvsp[0].tinfo)->cvalue;
                        }
#line 2012 "parser.tab.c"
    break;

  case 113: /* constant: STRINGCONST  */
#line 563 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Char;
                           (yyval.tree)->isArray = true;
                           (yyval.tree)->attr.string = strdup((yyvsp[0].tinfo)->svalue);
                        }
#line 2023 "parser.tab.c"
    break;

  case 114: /* constant: BOOLCONST  */
#line 570 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Boolean;
                           (yyval.tree)->attr.value = (yyvsp[0].tinfo)->nvalue;
                        }
#line 2033 "parser.tab.c"
    break;


#line 2037 "parser.tab.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

#line 576 "parser.y"


void yyerror (const char *msg)
{
   cout << "Error: " <<  msg << endl;
}

int main(int argc, char **argv) {
   int option, index;
   char *file = NULL;
   extern FILE *yyin;
   while ((option = getopt (argc, argv, "")) != -1)
      switch (option)
      {
      default:
         ;
      }
   if ( optind == argc ) yyparse();
   for (index = optind; index < argc; index++)
   {
      yyin = fopen (argv[index], "r");
      yyparse();
      printTree(stdout, syntaxTree, true, true);
      fprintf(stdout, "Number of warnings: 0\n");
      fprintf(stdout, "Number of errors: 0\n");
      fclose (yyin);
   }
   return 0;
}

