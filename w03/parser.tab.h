/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_PARSER_TAB_H_INCLUDED
# define YY_YY_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    DEC = 258,                     /* DEC  */
    INC = 259,                     /* INC  */
    MIN = 260,                     /* MIN  */
    MAX = 261,                     /* MAX  */
    MULASS = 262,                  /* MULASS  */
    DIVASS = 263,                  /* DIVASS  */
    ADDASS = 264,                  /* ADDASS  */
    SUBASS = 265,                  /* SUBASS  */
    NEQ = 266,                     /* NEQ  */
    GEQ = 267,                     /* GEQ  */
    LEQ = 268,                     /* LEQ  */
    EQ = 269,                      /* EQ  */
    AND = 270,                     /* AND  */
    OR = 271,                      /* OR  */
    NOT = 272,                     /* NOT  */
    BREAK = 273,                   /* BREAK  */
    RETURN = 274,                  /* RETURN  */
    STATIC = 275,                  /* STATIC  */
    IF = 276,                      /* IF  */
    THEN = 277,                    /* THEN  */
    ELSE = 278,                    /* ELSE  */
    FOR = 279,                     /* FOR  */
    TO = 280,                      /* TO  */
    BY = 281,                      /* BY  */
    DO = 282,                      /* DO  */
    WHILE = 283,                   /* WHILE  */
    CHAR = 284,                    /* CHAR  */
    INT = 285,                     /* INT  */
    BOOL = 286,                    /* BOOL  */
    PRECOMPILER = 287,             /* PRECOMPILER  */
    NUMCONST = 288,                /* NUMCONST  */
    CHARCONST = 289,               /* CHARCONST  */
    BOOLCONST = 290,               /* BOOLCONST  */
    STRINGCONST = 291,             /* STRINGCONST  */
    ID = 292,                      /* ID  */
    ERROR = 293                    /* ERROR  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 52 "parser.y"

   TokenData * tinfo;
   TreeNode * tree;
   ExpType expType;


#line 109 "parser.tab.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_PARSER_TAB_H_INCLUDED  */
