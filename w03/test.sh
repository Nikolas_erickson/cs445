make
cm=$?
if [[ "$cm" == "0" ]]; then
 echo "make successful"
else
 echo "ERROR DURING MAKE"
 exit
fi

dir=../bC_in_3
cd $dir
for i in *.bC
do
 diff -y <(../w03/bC $i) <(../bC 3 $i) >/dev/null
 cm=$?
  if [[ "$cm" == "1" ]]; then
  (echo " - failed $i, check command below";  echo "diff -y <(../w03/bC $dir/$i) <(../bC 3 $dir/$i) >/dev/null")
  ../w03/bC $i > got.txt
  ../bC 3 $i > exp.txt
  diff -y <(../w03/bC $i) <(../bC 3 $i)
  cd -
  mv $dir/got.txt .
  mv $dir/exp.txt .
  exit
 else
  (echo " - passed $dir/$i")
 fi
done
