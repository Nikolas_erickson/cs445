#include "treeUtils.h"
#include <string.h>
#include <iostream>

using namespace std;

TreeNode *newDeclNode(DeclKind kind, ExpType type, TokenData *token, TreeNode *c0, TreeNode *c1, TreeNode *c2)
{
   TreeNode *newNode = new TreeNode;
   newNode->child[0] = c0;
   newNode->child[1] = c1;
   newNode->child[2] = c2;
   newNode->nodekind = DeclK;
   newNode->kind.decl = kind;
   newNode->lineno = token->linenum;
   newNode->attr.name = strdup(token->svalue);
   newNode->type = type;

   // for functions with undefined types, set to void type
   if(kind == FuncK && type == UndefinedType)
   {
      newNode->type = Void;
   }
   // for variables with undefined types, set to type int
   if(kind == VarK && type == UndefinedType)
   {
      newNode->type = Integer;
   }

   return newNode;
}

TreeNode *newStmtNode(StmtKind kind, TokenData *token, TreeNode *c0, TreeNode *c1, TreeNode *c2)
{
   TreeNode *newNode = new TreeNode;
   newNode->child[0] = c0;
   newNode->child[1] = c1;
   newNode->child[2] = c2;
   newNode->nodekind = StmtK;
   newNode->kind.stmt = kind;
   newNode->lineno = token->linenum;
   return newNode;
}

TreeNode *newExpNode(ExpKind kind, TokenData *token, TreeNode *c0, TreeNode *c1, TreeNode *c2)
{
   TreeNode *newNode = new TreeNode;
   newNode->child[0] = c0;
   newNode->child[1] = c1;
   newNode->child[2] = c2;
   newNode->nodekind = ExpK;
   newNode->kind.exp = kind;
   newNode->lineno = token->linenum;
   newNode->attr.op = token->tokenclass;
   newNode->attr.string = strdup(token->svalue);
   return newNode;
}

static void printSpaces(FILE *listing, int depth)
{
    for (int i=0; i<depth; i++) fprintf(listing, ".   ");
}

void printTreeNode(FILE *listing, TreeNode *tree, bool showExpType, bool showAllocation)
{
   switch(tree->nodekind)
   {
      case DeclK:
      {
         // its a declaration
         char* expTypeStr = expTypeToStr(tree->type);
         switch(tree->kind.decl)
         { //VarK, FuncK, ParamK
            case FuncK:
            {
               fprintf(listing, "Func: %s returns type %s [line: %d]", tree->attr.name, expTypeStr, tree->lineno);
               break;
            }
            case VarK:
            {
               fprintf(listing, "Var: %s ", tree->attr.name);
               fprintf(listing,  "of ");
               if(tree->isStatic)
               {
                  fprintf(listing, "static ");
               }
               if(tree->isArray)
               {
                  fprintf(listing, "array of ");
               }
               fprintf(listing, "type %s [line: %d]", expTypeStr, tree->lineno);
               break;
            }
            case ParamK:
            {
               if(tree->isArray)
               {
                  fprintf(listing, "Parm: %s of array of type %s [line: %d]", tree->attr.name, expTypeStr, tree->lineno);
               }
               else
               {
                  fprintf(listing, "Parm: %s of type %s [line: %d]", tree->attr.name, expTypeStr, tree->lineno);
               }
               break;
            }
            default:
               break;

         }
         free(expTypeStr);
         break;
      }
      case StmtK:
      {
         // its a statement node
         switch(tree->kind.stmt)
         { ///IfK, WhileK, ForK, CompoundK, ReturnK, BreakK, RangeK
            case IfK:
            {
               fprintf(listing, "If [line: %d]", tree->lineno);
               break;
            }
            case WhileK:
            {
               fprintf(listing, "While [line: %d]", tree->lineno);
               break;
            }
            case ForK:
            {
               fprintf(listing, "For [line: %d]", tree->lineno);
               break;
            }
            case CompoundK:
            {
               fprintf(listing, "Compound [line: %d]", tree->lineno);
               break;
            }
            case ReturnK:
            {
               fprintf(listing, "Return [line: %d]", tree->lineno);
               break;
            }
            case BreakK:
            {
               fprintf(listing, "Break [line: %d]", tree->lineno);
               break;
            }
            case RangeK:
            {
               fprintf(listing, "Range [line: %d]", tree->lineno);
               break;
            }
         }
         break;
      }
      case ExpK:
      {
         // its an expression node
         switch(tree->kind.exp)
         { //AssignK, CallK, ConstantK, IdK, OpK
            case AssignK:
            {
               fprintf(listing, "Assign: %s [line: %d]",tree->attr.string, tree->lineno);
               break;
            }
            case CallK:
            {
               fprintf(listing, "Call: %s [line: %d]",tree->attr.string, tree->lineno);
               break;
            }
            case ConstantK:
            {
               fprintf(listing, "Const ");
               switch(tree->type)
               {
                  case Integer:
                  {
                     fprintf(listing, "%d ", tree->attr.value);
                     break;
                  }
                  case Boolean:
                  {
                     if(tree->attr.value)
                     {
                        fprintf(listing, "true ");
                     }
                     else
                     {
                        fprintf(listing, "false ");
                     }
                     break;
                  }
                  case Char:
                  {
                     if(tree->isArray)      //if it is string
                     {
                        fprintf(listing, "%s ", tree->attr.string);
                     }
                     else
                     {
                        fprintf(listing, "'%c' ", tree->attr.value);
                     }
                     break;
                  }
               }
               fprintf(listing, "[line: %d]",tree->lineno);
               break;
            }
            case IdK:
            {
               fprintf(listing, "Id: %s [line: %d]",tree->attr.string, tree->lineno);
               break;
            }
            case OpK:
            {
               fprintf(listing, "Op: %s [line: %d]",tree->attr.string, tree->lineno);
               break;
            }
         }
         break;
      }
   }
}

char* expTypeToStr(ExpType t)
{
   switch(t)
   {
      case Void:
         return strdup("void");
      case Integer:
         return strdup("int");
      case Boolean:
         return strdup("bool");
      case Char:
         return strdup("char");
      case UndefinedType:
         return strdup("undefinedType");
      default:
         return strdup("ERROR: UNKNOWN TYPE (function expTypeToStr)");
   }
}


void printTreeRec(FILE *listing, int depth, int siblingCnt, TreeNode *tree, bool showExpType, bool showAllocation)
{
   int childCnt;
   if (tree!=NULL) {
      // print self
      printTreeNode(listing, tree, showExpType, showAllocation);
      fprintf(listing, "\n");

      // print children
      for (childCnt = 0; childCnt<MAXCHILDREN; childCnt++) {
         if (tree->child[childCnt]) {
            printSpaces(listing, depth);
            fprintf(listing, "Child: %d  ", childCnt);
            printTreeRec(listing, depth+1, 1, tree->child[childCnt], showExpType, showAllocation);
         }
      }
   }
   // print sibling
   tree = tree->sibling;
   if (tree!=NULL) {
      if (depth) {
         printSpaces(listing, depth-1);
         fprintf(listing, "Sibling: %d  ", siblingCnt);
      }
      printTreeRec(listing, depth, siblingCnt+1, tree, showExpType, showAllocation);
   }
   fflush(listing);
}

void printTree(FILE *listing, TreeNode *tree, bool showExpType, bool showAllocation)
{
   if(tree == NULL){
      printf("NULL tree");
      return;
   }
   printTreeRec(listing, 1, 1, tree, showExpType, showAllocation);
}


