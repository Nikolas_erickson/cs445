#include "codegen.h"
#include "emitcode.h"
#include "parser.tab.h"


extern int numErrors;
extern int numWarnings;
//extern void yyparse();
extern int yydebug;
extern TreeNode * syntaxTree;
extern char ** largerTokens;
extern void initTokenStrings();


// These offsets that never change
#define OFPOFF 0
#define RETURNOFFSET -1


int toffset; // next available temporary space
FILE * code; // shared global code – already included
static bool linenumFlag; // mark with line numbers
static int breakloc; // which while to break to
static SymbolTable * globals; // the global symbol table

void codegenFull(TreeNode * tree);
void cgExpr(TreeNode * tree);
void cgDecl(TreeNode * tree);
void cgStmt(TreeNode * tree);

int offsetRegister(VarKind v)
{
   switch(v)
   {
      case Local: return FP;
      case Parameter: return FP;
      case Global: return GP;
      case LocalStatic: return GP;
      default:
         printf((char *) "ERROR(codegen): looking up offset register for a variable of type %d\n", v);
         return 666;
   }
}

void commentLineNum(TreeNode * tree)
{
   char buf[16];
   if(linenumFlag)
   {
      sprintf(buf, "%d", tree->lineno);
      emitComment((char *) "Line: ", buf);
   }
}

void codegenLibraryFun(TreeNode * tree)
{
   emitComment((char *) "");
   emitComment((char *) "** ** ** ** ** ** ** ** ** ** ** **");
   emitComment((char *) "FUNCTION", tree->attr.name);
   // remember where this function is
   tree->offset = emitSkip(0);
   // Store return address
   emitRM((char *) "ST", AC, RETURNOFFSET, FP, (char *) "Store return address");
   if(strcmp(tree->attr.name, (char *) "input") == 0)
   {
      emitRO((char *) "IN", RT, RT, RT, (char *) "Grab int input");
   }
   else if(strcmp(tree->attr.name, (char *) "inputb") == 0)
   {
      emitRO((char *) "INB", RT, RT, RT, (char *) "Grab bool input");
   }
   else if(strcmp(tree->attr.name, (char *) "inputc") == 0)
   {
      emitRO((char *) "INC", RT, RT, RT, (char *) "Grab char input");
   }
   else if(strcmp(tree->attr.name, (char *) "output") == 0)
   {
      emitRM((char *) "LD", AC, -2, FP, (char *) "Load parameter");
      emitRO((char *) "OUT", AC, AC, AC, (char *) "Output integer");
   }
   else if(strcmp(tree->attr.name, (char *) "outputb") == 0)
   {
      emitRM((char *) "LD", AC, -2, FP, (char *) "Load parameter");
      emitRO((char *) "OUTB", AC, AC, AC, (char *) "Output bool");
   }
   else if(strcmp(tree->attr.name, (char *) "outputc") == 0)
   {
      emitRM((char *) "LD", AC, -2, FP, (char *) "Load parameter");
      emitRO((char *) "OUTC", AC, AC, AC, (char *) "Output char");
   }
   else if(strcmp(tree->attr.name, (char *) "outnl") == 0)
   {
      emitRO((char *) "OUTNL", AC, AC, AC, (char *) "Output a newline");
   }
   else
   {
      emitComment((char *) "ERROR(LINKER): No support for special function");
      emitComment(tree->attr.name);
   }
   emitRM((char *) "LD", AC, RETURNOFFSET, FP, (char *) "Load return address");
   emitRM((char *) "LD", FP, OFPOFF, FP, (char *) "Adjust fp");
   emitGoto(0, AC, (char *) "Return");
   emitComment((char *) "END FUNCTION", tree->attr.name);
}

void codegenFun(TreeNode * tree)
{
   emitComment((char *) "");
   emitComment((char *) "** ** ** ** ** ** ** ** ** ** ** **");
   emitComment((char *) "FUNCTION", tree->attr.name);

   toffset = tree->size;
   emitComment((char *) "TOFF set:", toffset);
   // remember where this function is
   tree->offset = emitSkip(0);
   // Store return address
   emitRM((char *) "ST", AC, RETURNOFFSET, FP, (char *) "Store return address");

   codegenFull(tree->child[1]); // generate code for the body

   // In case there was no return statement
   // set return register to 0 and return
   emitComment((char *) "Add standard closing in case there is no return statement");
   emitRM((char *) "LDC", RT, 0, 6, (char *) "Set return value to 0");
   emitRM((char *) "LD", AC, RETURNOFFSET, FP, (char *) "Load return address");
   emitRM((char *) "LD", FP, OFPOFF, FP, (char *) "Adjust fp");
   emitGoto(0, AC, (char *) "Return");
   emitComment((char *) "END FUNCTION", tree->attr.name);

}

void cgDecl(TreeNode * tree)
{
   if(!tree)
   {
      return;
   }

   commentLineNum(tree);
   switch(tree->kind.decl)
   {
      case VarK:
         if(tree->isArray)
         {
            switch(tree->varKind)
            {
               case Local:
                  emitRM((char *) "LDC", AC, tree->size - 1, 6, (char *) "load size of array", tree->attr.name);
                  emitRM((char *) "ST", AC, tree->offset + 1, offsetRegister(tree->varKind),
                     (char *) "save size of array", tree->attr.name);
                  break;
               case LocalStatic:
               case Parameter:
               case Global:
                  // do nothing here
                  break;
               case None:
                  // Error Condition
                  break;
            }
            // ARRAY VALUE initialization
            if(tree->child[0])
            {
               cgExpr(tree->child[0]);
               emitRM((char *) "LDA", AC1, tree->offset, offsetRegister(tree->varKind), (char *) "address of lhs");
               emitRM((char *) "LD", AC2, 1, AC, (char *) "size of rhs");
               emitRM((char *) "LD", AC3, 1, AC1, (char *) "size of lhs");
               emitRO((char *) "SWP", AC2, AC3, 6, (char *) "pick smallest size");
               emitRO((char *) "MOV", AC1, AC, AC2, (char *) "array op =");
            }
         }
         else
         { // !tree->isArray
         // SCALAR VALUE initialization
            if(tree->child[0])
            {
               switch(tree->varKind)
               {
                  case Local:
                     // compute rhs -> AC;
                     cgExpr(tree->child[0]);
                     // save it
                     emitRM((char *) "ST", AC, tree->offset, FP, (char *) "Store variable", tree->attr.name);
                  case LocalStatic:
                  case Parameter:
                  case Global:
                     // do nothing here
                     break;
                  case None:
                     ///Error condition!!!
                     break;
               }
            }
         }
         break;
      case FuncK:
         if(tree->lineno == -1)
         { // These are the library functions we just added
            codegenLibraryFun(tree);
         }
         else
         {
            codegenFun(tree);
         }
         break;
      case ParamK:
         printf("PARAMK\n");
         // IMPORTANT: no instructions need to be allocated for parameters here
         break;
   }
}

void printnode(TreeNode * tree)
{
   if(tree)
   {
      printf("tree->attr.name: %s\n", tree->attr.name);
      printf("tree->attr.value: %d\n", tree->attr.value);
      printf("tree->attr.cvalue: %c\n", tree->attr.cvalue);
      printf("tree->attr.string: %s\n", tree->attr.string);
      printf("tree->isArray: %d\n", tree->isArray);
      printf("tree->size: %d\n", tree->size);
      printf("tree->offset: %d\n", tree->offset);
      printf("tree->lineno: %d\n", tree->lineno);
      printf("tree->nodekind: %d\n", tree->nodekind);
      printf("offset register: %d\n", offsetRegister(tree->varKind));
      printf("current toffset: %d\n", toffset);

   }
   else
   {
      printf("Node: NULL\n");
   }
}

void cgExpr(TreeNode * tree)
{
   if(!tree)
   {
      return;
   }
   switch(tree->kind.exp)
   {
      case AssignK:
         if(tree->child[0]->attr.op == '[')
         {
            TreeNode * rhs, * lhs, * var, * offset;
            lhs = tree->child[0];
            rhs = tree->child[1];
            var = lhs->child[0];
            offset = lhs->child[1];

            //printnode(var);
            cgExpr(offset); // load offset
            if(rhs)
            {
               emitRM((char *) "ST", AC, toffset, offsetRegister(var->varKind), (char *) "Push index");
               toffset--; emitComment((char *) "TOFF dec:", toffset);
               cgExpr(rhs); // load right side into AC
               toffset++; emitComment((char *) "TOFF inc:", toffset);
               emitRM((char *) "LD", AC1, toffset, offsetRegister(var->varKind), (char *) "Pop index"); // pop lhs into AC1
            }
            switch(var->varKind)
            {
               case Parameter:
                  emitRM((char *) "LD", AC2, var->offset, offsetRegister(var->varKind), (char *) "Load address of base of array", var->attr.name);
                  break;
               case LocalStatic:
               case Global:
               case Local:
                  emitRM((char *) "LDA", AC2, var->offset, offsetRegister(var->varKind), (char *) "Load address of base of array", var->attr.name);
                  break;
               default:
                  break;
            }
            if(rhs)
            {
               emitRO((char *) "SUB", AC2, AC2, AC1, (char *) "Compute offset of value", (char *) ""); // add offset from AC1 with base address of array
            }
            else
            {
               emitRO((char *) "SUB", AC2, AC2, AC, (char *) "Compute offset of value", (char *) ""); // add offset from AC with base address of array
            }
            switch(tree->attr.op)
            {
               case INC:
                  emitRM((char *) "LD", AC, 0, AC2, (char *) "load lhs variable", var->attr.name); // load value of array at index offset
                  emitRM((char *) "LDA", AC, 1, AC, (char *) "increment value of", var->attr.name); // add 1 to value
                  emitRM((char *) "ST", AC, 0, AC2, (char *) "Store variable", var->attr.name); // store value back in array
                  break;
               case DEC:
                  emitRM((char *) "LD", AC, 0, AC2, (char *) "load lhs variable", var->attr.name); // load value of array at index offset
                  emitRM((char *) "LDA", AC, -1, AC, (char *) "decrement value of", var->attr.name); // add 1 to value
                  emitRM((char *) "ST", AC, 0, AC2, (char *) "Store variable", var->attr.name); // store value back in array
                  break;
               case ADDASS:
                  emitRM((char *) "LD", AC1, 0, AC2, (char *) "load lhs variable", var->attr.name); // load value of array at index offset
                  emitRO((char *) "ADD", AC, AC1, AC, (char *) "op +="); // add value
                  emitRM((char *) "ST", AC, 0, AC2, (char *) "Store variable", var->attr.name); // store value back in array
                  break;
               case DIVASS:
                  emitRM((char *) "LD", AC1, 0, AC2, (char *) "load lhs variable", var->attr.name); // load value of array at index offset
                  emitRO((char *) "DIV", AC, AC1, AC, (char *) "op /="); // divide value
                  emitRM((char *) "ST", AC, 0, AC2, (char *) "Store variable", var->attr.name); // store value back in array
                  break;
               case MULASS:
                  emitRM((char *) "LD", AC1, 0, AC2, (char *) "load lhs variable", var->attr.name); // load value of array at index offset
                  emitRO((char *) "MUL", AC, AC1, AC, (char *) "op *="); // divide value
                  emitRM((char *) "ST", AC, 0, AC2, (char *) "Store variable", var->attr.name); // store value back in array
                  break;
               case SUBASS:
                  emitRM((char *) "LD", AC1, 0, AC2, (char *) "load lhs variable", var->attr.name); // load value of array at index offset
                  emitRO((char *) "SUB", AC, AC1, AC, (char *) "op -="); // divide value
                  emitRM((char *) "ST", AC, 0, AC2, (char *) "Store variable", var->attr.name); // store value back in array
                  break;
            }
         }
         else
         {
            cgExpr(tree->child[1]); // load right side into AC
            int offReg;
            offReg = offsetRegister(tree->child[0]->varKind);
            // Lots of cases that use it. Here is a sample:
            switch(tree->attr.op)
            {
               case '=':
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
               case ADDASS:
                  emitRM((char *) "LD", AC1, tree->child[0]->offset, offReg,
                     (char *) "load lhs variable", tree->child[0]->attr.name);
                  emitRO((char *) "ADD", AC, AC1, AC, (char *) "op +=");
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
               case SUBASS:
                  emitRM((char *) "LD", AC1, tree->child[0]->offset, offReg,
                     (char *) "load lhs variable", tree->child[0]->attr.name);
                  emitRO((char *) "SUB", AC, AC1, AC, (char *) "op -=");
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
               case MULASS:
                  emitRM((char *) "LD", AC1, tree->child[0]->offset, offReg,
                     (char *) "load lhs variable", tree->child[0]->attr.name);
                  emitRO((char *) "MUL", AC, AC1, AC, (char *) "op *=");
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
               case DIVASS:
                  emitRM((char *) "LD", AC1, tree->child[0]->offset, offReg,
                     (char *) "load lhs variable", tree->child[0]->attr.name);
                  emitRO((char *) "DIV", AC, AC1, AC, (char *) "op /=");
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
               case INC:
                  emitRM((char *) "LD", AC, tree->child[0]->offset, offReg,
                     (char *) "load lhs variable", tree->child[0]->attr.name);
                  emitRM((char *) "LDA", AC, 1, AC, (char *) "increment value of", (char *) "i");
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
               case DEC:
                  emitRM((char *) "LD", AC, tree->child[0]->offset, offReg,
                     (char *) "load lhs variable", tree->child[0]->attr.name);
                  emitRM((char *) "LDA", AC, -1, AC, (char *) "decrement value of", (char *) "i");
                  emitRM((char *) "ST", AC, tree->child[0]->offset, offReg,
                     (char *) "Store variable", tree->child[0]->attr.name);
                  break;
            }
         }

         break;
      case CallK:
         emitComment((char *) "CALL", tree->attr.name);
         emitRM((char *) "ST", FP, toffset, FP, (char *) "Store fp in ghost frame for", tree->attr.name);
         toffset--; emitComment((char *) "TOFF dec:", toffset);
         toffset--; emitComment((char *) "TOFF dec:", toffset);
         if(tree->child[0])
         {
            //do params
            emitComment((char *) "Param start", tree->attr.name);
         }
         emitComment((char *) "Param end", tree->attr.name);
         emitRM((char *) "LDA", FP, toffset + 2, FP, (char *) "Ghost frame becomes new active frame");
         emitRM((char *) "LDA", AC, 1, PC, (char *) "Return address in ac");
         emitGotoAbs(tree->offset, (char *) "CALL", tree->attr.name);
         //printf("emit skip %d: ", emitSkip(0)); // results in 44
         emitComment((char *) "Call end", tree->attr.name);
         break;
      case ConstantK:
         if(tree->type == Char)
         {
            if(tree->isArray)
            {
               emitStrLit(tree->offset, tree->attr.string);
               emitRM((char *) "LDA", AC, tree->offset, 0, (char *) "Load address of char array");
            }
            else
            {
               emitRM((char *) "LDC", AC, int(tree->attr.cvalue), 6, (char *) "Load char constant");
            }
            break;
         }
         else if(tree->type == Boolean)
         {
            if(tree->isArray)
            {
               emitStrLit(tree->offset, tree->attr.string);
               emitRM((char *) "LDA", AC, tree->offset, 0, (char *) "Load address of Boolean array");
            }
            else
            {
               emitRM((char *) "LDC", AC, int(tree->attr.value), 6, (char *) "Load Boolean constant");
            }
            break;
         }
         else if(tree->type == Integer)
         {
            if(tree->isArray)
            {
               emitStrLit(tree->offset, tree->attr.string);
               emitRM((char *) "LDA", AC, tree->offset, 0, (char *) "Load address of integer array");
            }
            else
            {
               emitRM((char *) "LDC", AC, int(tree->attr.value), 6, (char *) "Load integer constant");
            }
            break;
         }
         break;
      case IdK:
         if(tree->isArray)
         {
            emitRM((char *) "LDA", AC, tree->offset, offsetRegister(tree->varKind), (char *) "Load address of base of array XXX", tree->attr.name);
         }
         else
         {
            emitRM((char *) "LD", AC, tree->offset, offsetRegister(tree->varKind), (char *) "Load variable", tree->attr.name);
         }
         break;
      case OpK:
         if(tree->child[1]) // is binary op
         {
            // this part seems unnecessary, can you not load directly from AC into AC1?     DRBC QUESTION
            cgExpr(tree->child[0]);
            emitRM((char *) "ST", AC, toffset, FP, (char *) "Push left side");
            toffset--; emitComment((char *) "TOFF dec:", toffset);
            cgExpr(tree->child[1]);
            toffset++; emitComment((char *) "TOFF inc:", toffset);
            emitRM((char *) "LD", AC1, toffset, FP, (char *) "Pop left into ac1");
            if(strcmp(tree->attr.name, "+") == 0)
            {
               emitRO((char *) "ADD", AC, AC1, AC, (char *) "Op +"); // DRBC QUESTION why am i adding a space??
            }
         }
         else // is unary operation: sizeof, chsign, not
         {
            printf("OPK PT2\n");
         }
         break;
      default:
         break;
   }
}

void printlocations(int skiploc, int skiploc2, int currloc)
{
   printf("breakloc: %d\n", breakloc);
   printf("toffset: %d\n", toffset);
   printf("globals: %p\n", globals);
   printf("currloc: %d\n", currloc);
   printf("skiploc: %d\n", skiploc);
   printf("skiploc2: %d\n", skiploc2);
}

void cgStmt(TreeNode * tree)
{
   if(!tree)
   {
      return;
   }
   // local state to remember stuff
   int skiploc = 0, skiploc2 = 0, currloc = 0; // some temporary instuction addresses
   TreeNode * loopindex = NULL; // a pointer to the index variable declaration node
   commentLineNum(tree);
   switch(tree->kind.stmt)
   {
      case IfK:
         printf("IFK\n");
         break;
      case WhileK:
         emitComment((char *) "WHILE");
         currloc = emitSkip(0); // return to here to do the test
         cgExpr(tree->child[0]); // test expression
         emitRM((char *) "JNZ", AC, 1, PC, (char *) "Jump to while part");
         emitComment((char *) "DO");
         skiploc = breakloc; // save the old break statement return point
         breakloc = emitSkip(1); // addr of instr that jumps to end of loop
         // this is also the backpatch point
         codegenFull(tree->child[1]); // do body of loop
         emitGotoAbs(currloc, (char *) "go to beginning of loop");
         backPatchAJumpToHere(breakloc, (char *) "Jump past loop [backpatch]");
         // backpatch jump to end of loop
         breakloc = skiploc; // restore for break statement
         emitComment((char *) "END WHILE");
         break;
      case ForK:
         printf("FORK\n");
         break;
      case ReturnK:
         emitComment((char *) "RETURN");
         emitRM((char *) "LD", AC, -1, FP, (char *) "Load return address");
         emitRM((char *) "LD", FP, 0, FP, (char *) "Adjust fp");
         emitRM((char *) "JMP", PC, 0, AC, (char *) "Return");
         break;
      case BreakK:
         emitComment((char *) "BREAK");
         //printlocations(skiploc, skiploc2, currloc);
         emitRM((char *) "JMP", PC, toffset, PC, (char *) "break"); // maybe toffset should be -2
         break;
      case RangeK:
         printf("RANGEK\n");
         break;
      case CompoundK:
      {
         int savedToffset;
         savedToffset = toffset;
         toffset = tree->size; // recover the end of activation record
         emitComment((char *) "COMPOUND");
         emitComment((char *) "TOFF set:", toffset);
         codegenFull(tree->child[0]); // process inits
         emitComment((char *) "Compound Body");
         codegenFull(tree->child[1]); // process body
         toffset = savedToffset;
         emitComment((char *) "TOFF set:", toffset);
         emitComment((char *) "END COMPOUND");
      }
      break;
      default:
         break;
   }
}
void codegenFull(TreeNode * tree)
{
   if(!tree)
   {
      return;
   }
   switch(tree->nodekind)
   {
      case DeclK:
         cgDecl(tree);
         break;
      case StmtK:
         cgStmt(tree);
         break;
      case ExpK:
         emitComment((char *) "EXPRESSION");
         cgExpr(tree);
         break;
      default:
         break;
   }
   codegenFull(tree->sibling);
}


void initAGlobalSymbol(std::string sym, void * ptr)
{
   TreeNode * currnode;
   // printf("Symbol: %s\n", sym.c_str()); // dump the symbol table
   currnode = (TreeNode *) ptr;
   // printf("lineno: %d\n", currnode->lineno); // dump the symbol table
   if(currnode->lineno != -1)
   {
      if(currnode->isArray)
      {
         emitRM((char *) "LDC", AC, currnode->size - 1, 6, (char *) "load size of array", currnode->attr.name);
         emitRM((char *) "ST", AC, currnode->offset + 1, GP, (char *) "save size of array", currnode->attr.name);
      }
      if(currnode->kind.decl == VarK &&
         (currnode->varKind == Global || currnode->varKind == LocalStatic))
      {
         if(currnode->child[0])
         {
            // compute rhs -> AC;
            cgExpr(currnode->child[0]);
            // save it
            emitRM((char *) "ST", AC, currnode->offset, GP,
               (char *) "Store variable", currnode->attr.name);
         }
      }
   }
}

void initGlobalArraySizes()
{
   emitComment((char *) "INIT GLOBALS AND STATICS");
   globals->applyToAllGlobal(initAGlobalSymbol);
   emitComment((char *) "END INIT GLOBALS AND STATICS");
}


// Generate init code ...
void codegenInit(int initJump, int globalOffset)
{
   backPatchAJumpToHere(initJump, (char *) "Jump to init [backpatch]");
   emitComment((char *) "INIT");
   //OLD pre 4.6 TM emitRM((char *)"LD", GP, 0, 0, (char *)"Set the global pointer");
   emitRM((char *) "LDA", FP, globalOffset, GP, (char *) "set first frame at end of globals");
   emitRM((char *) "ST", FP, 0, FP, (char *) "store old fp (point to self)");
   initGlobalArraySizes();
   emitRM((char *) "LDA", AC, 1, PC, (char *) "Return address in ac");
   { // jump to main
      TreeNode * funcNode;
      funcNode = (TreeNode *) (globals->lookup((char *) "main"));
      if(funcNode)
      {
         emitGotoAbs(funcNode->offset, (char *) "Jump to main");
      }
      else
      {
         printf((char *) "ERROR(LINKER): Procedure main is not defined.\n");
         numErrors++;
      }
   }
   emitRO((char *) "HALT", 0, 0, 0, (char *) "DONE!");
   emitComment((char *) "END INIT");
}

void codegen(FILE * codeIn,          // where the code should be written
   char * srcFile,         // name of file compiled
   TreeNode * syntaxTree,  // tree to process
   SymbolTable * globalsIn,     // globals so function info can be found
   int & globalOffset,      // size of the global frame
   bool linenumFlagIn)   // comment with line numbers
{
   int initJump;

   code = codeIn;
   globals = globalsIn;
   linenumFlag = linenumFlagIn;
   breakloc = 0;

   initJump = emitSkip(1); // save a place for the jump to init

   emitComment((char *) "bC compiler version bC-Su23");
   emitComment((char *) "File compiled: ", srcFile);

   codegenFull(syntaxTree); // general code generation including I/O library

   codegenInit(initJump, globalOffset); // generation of initialization for run
}