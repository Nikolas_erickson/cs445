/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "parser.y"

#include <cstdio>
#include <iostream>
#include <unistd.h>

#include "scanType.h"
#include "treeNodes.h"
#include "treeUtils.h"
#include "symbolTable.h"
#include "semantics.h"
#include "codegen.h"

#include "yyerror.h"



int numErrors=0;
int numWarnings=0;

extern int line;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;

static TreeNode * syntaxTree; /* stores syntax tree for later return */


using namespace std;

void printToken(TokenData myData, string tokenName, int type = 0) {
   cout << "Line: " << myData.linenum << " Type: " << tokenName;
   if(type==0)
     cout << " Token: " << myData.tokenstr;
   if(type==1)
     cout << " Token: " << myData.nvalue;
   if(type==2)
     cout << " Token: " << myData.cvalue;
   cout << endl;
}

TreeNode* makeRangeIntoFor(TokenData* mutId, TreeNode* rangeNode, TreeNode* stmtNode){
   TreeNode* forNode = newStmtNode(ForK, mutId);
   forNode->child[0] = newDeclNode(VarK, Integer, mutId);
   forNode->child[1] = rangeNode;
   forNode->child[2] = stmtNode;


   return forNode;
}

TreeNode* addSibling(TreeNode *tree, TreeNode *sib)
{
   if(tree != NULL)
   {
      TreeNode * t = tree;
      while( t->sibling != NULL )
      {
         t = t->sibling;
      }
      t->sibling = sib;
      return tree;
   }
   else
   {
      return sib;
   }
}

void printDebug(const char* st)
{
   if(1)
   {
      cout << st << endl;
   }
}


#line 150 "parser.tab.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "parser.tab.h"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_FIRSTOP = 3,                    /* FIRSTOP  */
  YYSYMBOL_ARITHOPSTART = 4,               /* ARITHOPSTART  */
  YYSYMBOL_5_ = 5,                         /* '+'  */
  YYSYMBOL_6_ = 6,                         /* '-'  */
  YYSYMBOL_7_ = 7,                         /* '/'  */
  YYSYMBOL_8_ = 8,                         /* '*'  */
  YYSYMBOL_MIN = 9,                        /* MIN  */
  YYSYMBOL_MAX = 10,                       /* MAX  */
  YYSYMBOL_DEC = 11,                       /* DEC  */
  YYSYMBOL_INC = 12,                       /* INC  */
  YYSYMBOL_ARITHOPEND = 13,                /* ARITHOPEND  */
  YYSYMBOL_RELOPSTART = 14,                /* RELOPSTART  */
  YYSYMBOL_15_ = 15,                       /* '<'  */
  YYSYMBOL_16_ = 16,                       /* '>'  */
  YYSYMBOL_LEQ = 17,                       /* LEQ  */
  YYSYMBOL_GEQ = 18,                       /* GEQ  */
  YYSYMBOL_EQ = 19,                        /* EQ  */
  YYSYMBOL_NEQ = 20,                       /* NEQ  */
  YYSYMBOL_RELOPEND = 21,                  /* RELOPEND  */
  YYSYMBOL_BOOLOPSTART = 22,               /* BOOLOPSTART  */
  YYSYMBOL_AND = 23,                       /* AND  */
  YYSYMBOL_OR = 24,                        /* OR  */
  YYSYMBOL_NOT = 25,                       /* NOT  */
  YYSYMBOL_BOOLOPEND = 26,                 /* BOOLOPEND  */
  YYSYMBOL_27_ = 27,                       /* '='  */
  YYSYMBOL_28_ = 28,                       /* '?'  */
  YYSYMBOL_29_ = 29,                       /* '%'  */
  YYSYMBOL_30_ = 30,                       /* '['  */
  YYSYMBOL_31_ = 31,                       /* ']'  */
  YYSYMBOL_32_ = 32,                       /* '{'  */
  YYSYMBOL_33_ = 33,                       /* '}'  */
  YYSYMBOL_34_ = 34,                       /* ','  */
  YYSYMBOL_35_ = 35,                       /* ';'  */
  YYSYMBOL_36_ = 36,                       /* ':'  */
  YYSYMBOL_37_ = 37,                       /* '('  */
  YYSYMBOL_38_ = 38,                       /* ')'  */
  YYSYMBOL_MULASS = 39,                    /* MULASS  */
  YYSYMBOL_DIVASS = 40,                    /* DIVASS  */
  YYSYMBOL_ADDASS = 41,                    /* ADDASS  */
  YYSYMBOL_SUBASS = 42,                    /* SUBASS  */
  YYSYMBOL_BREAK = 43,                     /* BREAK  */
  YYSYMBOL_RETURN = 44,                    /* RETURN  */
  YYSYMBOL_STATIC = 45,                    /* STATIC  */
  YYSYMBOL_IF = 46,                        /* IF  */
  YYSYMBOL_THEN = 47,                      /* THEN  */
  YYSYMBOL_ELSE = 48,                      /* ELSE  */
  YYSYMBOL_FOR = 49,                       /* FOR  */
  YYSYMBOL_TO = 50,                        /* TO  */
  YYSYMBOL_BY = 51,                        /* BY  */
  YYSYMBOL_DO = 52,                        /* DO  */
  YYSYMBOL_WHILE = 53,                     /* WHILE  */
  YYSYMBOL_PRECOMPILER = 54,               /* PRECOMPILER  */
  YYSYMBOL_LASTOP = 55,                    /* LASTOP  */
  YYSYMBOL_CHAR = 56,                      /* CHAR  */
  YYSYMBOL_INT = 57,                       /* INT  */
  YYSYMBOL_BOOL = 58,                      /* BOOL  */
  YYSYMBOL_NUMCONST = 59,                  /* NUMCONST  */
  YYSYMBOL_CHARCONST = 60,                 /* CHARCONST  */
  YYSYMBOL_BOOLCONST = 61,                 /* BOOLCONST  */
  YYSYMBOL_STRINGCONST = 62,               /* STRINGCONST  */
  YYSYMBOL_ID = 63,                        /* ID  */
  YYSYMBOL_ERROR = 64,                     /* ERROR  */
  YYSYMBOL_LASTTERM = 65,                  /* LASTTERM  */
  YYSYMBOL_YYACCEPT = 66,                  /* $accept  */
  YYSYMBOL_program = 67,                   /* program  */
  YYSYMBOL_precomList = 68,                /* precomList  */
  YYSYMBOL_declList = 69,                  /* declList  */
  YYSYMBOL_decl = 70,                      /* decl  */
  YYSYMBOL_varDecl = 71,                   /* varDecl  */
  YYSYMBOL_scopedVarDecl = 72,             /* scopedVarDecl  */
  YYSYMBOL_varDeclList = 73,               /* varDeclList  */
  YYSYMBOL_varDeclInit = 74,               /* varDeclInit  */
  YYSYMBOL_varDeclId = 75,                 /* varDeclId  */
  YYSYMBOL_typeSpec = 76,                  /* typeSpec  */
  YYSYMBOL_funDecl = 77,                   /* funDecl  */
  YYSYMBOL_parms = 78,                     /* parms  */
  YYSYMBOL_parmList = 79,                  /* parmList  */
  YYSYMBOL_parmTypeList = 80,              /* parmTypeList  */
  YYSYMBOL_parmIdList = 81,                /* parmIdList  */
  YYSYMBOL_parmId = 82,                    /* parmId  */
  YYSYMBOL_stmt = 83,                      /* stmt  */
  YYSYMBOL_matched = 84,                   /* matched  */
  YYSYMBOL_iterRange = 85,                 /* iterRange  */
  YYSYMBOL_unmatched = 86,                 /* unmatched  */
  YYSYMBOL_expStmt = 87,                   /* expStmt  */
  YYSYMBOL_compoundStmt = 88,              /* compoundStmt  */
  YYSYMBOL_localDecls = 89,                /* localDecls  */
  YYSYMBOL_stmtList = 90,                  /* stmtList  */
  YYSYMBOL_returnStmt = 91,                /* returnStmt  */
  YYSYMBOL_breakStmt = 92,                 /* breakStmt  */
  YYSYMBOL_exp = 93,                       /* exp  */
  YYSYMBOL_assignop = 94,                  /* assignop  */
  YYSYMBOL_simpleExp = 95,                 /* simpleExp  */
  YYSYMBOL_andExp = 96,                    /* andExp  */
  YYSYMBOL_unaryRelExp = 97,               /* unaryRelExp  */
  YYSYMBOL_relExp = 98,                    /* relExp  */
  YYSYMBOL_relop = 99,                     /* relop  */
  YYSYMBOL_minmaxExp = 100,                /* minmaxExp  */
  YYSYMBOL_minmaxop = 101,                 /* minmaxop  */
  YYSYMBOL_sumExp = 102,                   /* sumExp  */
  YYSYMBOL_sumop = 103,                    /* sumop  */
  YYSYMBOL_mulExp = 104,                   /* mulExp  */
  YYSYMBOL_mulop = 105,                    /* mulop  */
  YYSYMBOL_unaryExp = 106,                 /* unaryExp  */
  YYSYMBOL_unaryop = 107,                  /* unaryop  */
  YYSYMBOL_factor = 108,                   /* factor  */
  YYSYMBOL_mutable = 109,                  /* mutable  */
  YYSYMBOL_immutable = 110,                /* immutable  */
  YYSYMBOL_call = 111,                     /* call  */
  YYSYMBOL_args = 112,                     /* args  */
  YYSYMBOL_argList = 113,                  /* argList  */
  YYSYMBOL_constant = 114                  /* constant  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if 1

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
# define YYCOPY_NEEDED 1
#endif /* 1 */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  14
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   230

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  66
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  49
/* YYNRULES -- Number of rules.  */
#define YYNRULES  114
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  176

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   302


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,    29,     2,     2,
      37,    38,     8,     5,    34,     6,     2,     7,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    36,    35,
      15,    27,    16,    28,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    30,     2,    31,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    32,     2,    33,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       9,    10,    11,    12,    13,    14,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   128,   128,   132,   138,   143,   150,   154,   158,   160,
     164,   175,   186,   197,   201,   206,   208,   211,   213,   220,
     222,   224,   228,   232,   237,   240,   242,   246,   249,   261,
     266,   269,   271,   277,   279,   283,   285,   287,   291,   292,
     294,   295,   297,   299,   303,   305,   307,   309,   314,   316,
     319,   324,   329,   332,   337,   340,   342,   345,   348,   353,
     358,   363,   365,   366,   367,   368,   369,   371,   376,   378,
     383,   385,   390,   392,   397,   399,   400,   401,   402,   403,
     404,   406,   411,   414,   415,   417,   422,   424,   425,   428,
     433,   436,   437,   438,   441,   454,   457,   458,   459,   462,
     463,   466,   471,   478,   480,   481,   483,   489,   491,   493,
     497,   499,   507,   514,   522
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if 1
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "FIRSTOP",
  "ARITHOPSTART", "'+'", "'-'", "'/'", "'*'", "MIN", "MAX", "DEC", "INC",
  "ARITHOPEND", "RELOPSTART", "'<'", "'>'", "LEQ", "GEQ", "EQ", "NEQ",
  "RELOPEND", "BOOLOPSTART", "AND", "OR", "NOT", "BOOLOPEND", "'='", "'?'",
  "'%'", "'['", "']'", "'{'", "'}'", "','", "';'", "':'", "'('", "')'",
  "MULASS", "DIVASS", "ADDASS", "SUBASS", "BREAK", "RETURN", "STATIC",
  "IF", "THEN", "ELSE", "FOR", "TO", "BY", "DO", "WHILE", "PRECOMPILER",
  "LASTOP", "CHAR", "INT", "BOOL", "NUMCONST", "CHARCONST", "BOOLCONST",
  "STRINGCONST", "ID", "ERROR", "LASTTERM", "$accept", "program",
  "precomList", "declList", "decl", "varDecl", "scopedVarDecl",
  "varDeclList", "varDeclInit", "varDeclId", "typeSpec", "funDecl",
  "parms", "parmList", "parmTypeList", "parmIdList", "parmId", "stmt",
  "matched", "iterRange", "unmatched", "expStmt", "compoundStmt",
  "localDecls", "stmtList", "returnStmt", "breakStmt", "exp", "assignop",
  "simpleExp", "andExp", "unaryRelExp", "relExp", "relop", "minmaxExp",
  "minmaxop", "sumExp", "sumop", "mulExp", "mulop", "unaryExp", "unaryop",
  "factor", "mutable", "immutable", "call", "args", "argList", "constant", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-128)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      19,  -128,  -128,  -128,  -128,   -23,    31,   167,    46,  -128,
    -128,   -19,  -128,    82,  -128,  -128,    46,  -128,   -17,    16,
    -128,    -3,   -10,    20,    29,  -128,     9,    82,     8,  -128,
     147,    65,    57,  -128,    99,    82,    75,    77,    83,  -128,
    -128,  -128,   147,  -128,   147,  -128,  -128,  -128,  -128,     0,
      93,    96,  -128,  -128,   171,    61,    10,  -128,   157,  -128,
    -128,  -128,  -128,  -128,    94,   -10,  -128,  -128,    91,   141,
     147,    67,   147,  -128,  -128,  -128,  -128,  -128,  -128,  -128,
     100,    93,    81,  -128,  -128,    99,  -128,    90,   147,   147,
     147,   147,  -128,  -128,  -128,  -128,  -128,  -128,  -128,  -128,
     157,   157,  -128,  -128,   157,  -128,  -128,  -128,   157,  -128,
    -128,  -128,   -35,  -128,  -128,   106,   -15,   119,   -18,  -128,
    -128,  -128,  -128,  -128,  -128,  -128,  -128,   147,  -128,  -128,
     120,  -128,   112,   122,    96,  -128,    69,    61,    10,  -128,
      82,  -128,     8,    37,  -128,    99,   147,    99,  -128,  -128,
    -128,   147,     8,    50,  -128,  -128,  -128,   109,   102,   -12,
    -128,  -128,  -128,    54,  -128,    99,    99,   147,  -128,  -128,
    -128,  -128,  -128,   -16,   147,    93
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     5,    21,    19,    20,     0,     0,     0,     3,     7,
       8,     0,     9,    25,     1,     4,     2,     6,    17,     0,
      14,    15,     0,     0,    24,    27,     0,    25,     0,    10,
       0,    31,    28,    30,     0,     0,     0,     0,    17,    13,
      96,    97,     0,    98,     0,   111,   112,   114,   113,   101,
      16,    68,    70,    72,    74,    82,    86,    90,     0,    95,
     100,    99,   104,   105,     0,     0,    52,    49,     0,     0,
       0,     0,     0,    23,    33,    34,    38,    39,    40,    41,
       0,    61,   100,    26,    18,     0,    71,     0,     0,   108,
       0,     0,    84,    83,    76,    77,    75,    78,    79,    80,
       0,     0,    87,    88,     0,    92,    91,    93,     0,    94,
      32,    29,    54,    57,    55,     0,     0,     0,     0,    48,
      60,    59,    62,    65,    66,    63,    64,     0,    22,   103,
       0,   110,     0,   107,    67,    69,    73,    81,    85,    89,
       0,    51,     0,     0,    56,     0,     0,     0,    58,   102,
     106,     0,     0,     0,    50,    53,    44,    33,     0,     0,
      36,    46,   109,     0,    12,     0,     0,     0,    11,    35,
      45,    37,    47,    42,     0,    43
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -128,  -128,  -128,   160,    11,  -128,  -128,  -127,   136,  -128,
     -11,  -128,   143,  -128,   133,  -128,   108,   -82,  -119,  -128,
    -106,  -128,  -128,  -128,  -128,  -128,  -128,   -40,  -128,   -30,
      84,   -37,  -128,  -128,    71,  -128,    76,  -128,    78,  -128,
     -51,  -128,  -128,   -33,  -128,  -128,  -128,  -128,  -128
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,     6,     7,     8,     9,    10,   141,    19,    20,    21,
      11,    12,    23,    24,    25,    32,    33,    73,    74,   158,
      75,    76,    77,   112,   143,    78,    79,    80,   127,    81,
      51,    52,    53,   100,    54,   101,    55,   104,    56,   108,
      57,    58,    59,    60,    61,    62,   132,   133,    63
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      50,    82,    22,   128,    87,    86,    90,   109,    90,    90,
     140,    82,    90,    26,    13,   153,    22,   105,   106,    17,
      27,     2,     3,     4,    22,   163,   157,    17,   160,   115,
      88,    14,   145,    30,   147,   174,    82,    89,   167,   107,
     116,   161,   118,    40,    18,    41,   169,   171,   130,   131,
      28,    29,    82,    31,   135,    82,    82,   139,    34,   170,
     172,   155,    42,   156,    35,    43,   102,   103,    36,    66,
     154,    38,    67,     1,    44,     2,     3,     4,    92,    93,
      68,    69,     5,    70,    28,   164,    71,   148,    28,   168,
      72,    65,   120,   121,    82,    64,    45,    46,    47,    48,
      49,   142,     2,     3,     4,    40,    84,    41,   122,     5,
      82,   162,    82,    26,    82,    85,   159,    90,    82,    91,
     123,   124,   125,   126,    42,   110,   113,    43,   129,   152,
     117,    66,    82,    82,    67,   119,    44,   173,     2,     3,
       4,   144,    68,    69,   175,    70,   146,    40,    71,    41,
     150,   149,    72,    40,   166,    41,   151,   165,    45,    46,
      47,    48,    49,    40,    39,    41,    42,    16,    83,    43,
      37,   136,    42,   111,   134,    43,   114,   137,    44,     0,
      92,    93,   138,     0,    44,    43,    94,    95,    96,    97,
      98,    99,     0,     0,    44,     0,     0,     0,     0,     0,
      45,    46,    47,    48,    49,     0,    45,    46,    47,    48,
      49,     0,     0,     0,     0,     0,    45,    46,    47,    48,
      49,    15,     0,     2,     3,     4,     0,     0,     0,     0,
       5
};

static const yytype_int16 yycheck[] =
{
      30,    34,    13,    85,    44,    42,    24,    58,    24,    24,
      45,    44,    24,    30,    37,   142,    27,     7,     8,     8,
      37,    56,    57,    58,    35,   152,   145,    16,   147,    69,
      30,     0,    47,    36,    52,    51,    69,    37,    50,    29,
      70,   147,    72,     6,    63,     8,   165,   166,    88,    89,
      34,    35,    85,    63,    91,    88,    89,   108,    38,   165,
     166,   143,    25,   145,    35,    28,     5,     6,    59,    32,
      33,    63,    35,    54,    37,    56,    57,    58,     9,    10,
      43,    44,    63,    46,    34,    35,    49,   127,    34,    35,
      53,    34,    11,    12,   127,    30,    59,    60,    61,    62,
      63,   112,    56,    57,    58,     6,    31,     8,    27,    63,
     143,   151,   145,    30,   147,    38,   146,    24,   151,    23,
      39,    40,    41,    42,    25,    31,    35,    28,    38,   140,
      63,    32,   165,   166,    35,    35,    37,   167,    56,    57,
      58,    35,    43,    44,   174,    46,    27,     6,    49,     8,
      38,    31,    53,     6,    52,     8,    34,    48,    59,    60,
      61,    62,    63,     6,    28,     8,    25,     7,    35,    28,
      27,   100,    25,    65,    90,    28,    35,   101,    37,    -1,
       9,    10,   104,    -1,    37,    28,    15,    16,    17,    18,
      19,    20,    -1,    -1,    37,    -1,    -1,    -1,    -1,    -1,
      59,    60,    61,    62,    63,    -1,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    59,    60,    61,    62,
      63,    54,    -1,    56,    57,    58,    -1,    -1,    -1,    -1,
      63
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    54,    56,    57,    58,    63,    67,    68,    69,    70,
      71,    76,    77,    37,     0,    54,    69,    70,    63,    73,
      74,    75,    76,    78,    79,    80,    30,    37,    34,    35,
      36,    63,    81,    82,    38,    35,    59,    78,    63,    74,
       6,     8,    25,    28,    37,    59,    60,    61,    62,    63,
      95,    96,    97,    98,   100,   102,   104,   106,   107,   108,
     109,   110,   111,   114,    30,    34,    32,    35,    43,    44,
      46,    49,    53,    83,    84,    86,    87,    88,    91,    92,
      93,    95,   109,    80,    31,    38,    97,    93,    30,    37,
      24,    23,     9,    10,    15,    16,    17,    18,    19,    20,
      99,   101,     5,     6,   103,     7,     8,    29,   105,   106,
      31,    82,    89,    35,    35,    93,    95,    63,    95,    35,
      11,    12,    27,    39,    40,    41,    42,    94,    83,    38,
      93,    93,   112,   113,    96,    97,   100,   102,   104,   106,
      45,    72,    76,    90,    35,    47,    27,    52,    93,    31,
      38,    34,    76,    73,    33,    83,    83,    84,    85,    95,
      84,    86,    93,    73,    35,    48,    52,    50,    35,    84,
      86,    84,    86,    95,    51,    95
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    66,    67,    67,    68,    68,    69,    69,    70,    70,
      71,    72,    72,    73,    73,    74,    74,    75,    75,    76,
      76,    76,    77,    77,    78,    78,    79,    79,    80,    81,
      81,    82,    82,    83,    83,    84,    84,    84,    84,    84,
      84,    84,    85,    85,    86,    86,    86,    86,    87,    87,
      88,    89,    89,    90,    90,    91,    91,    92,    93,    93,
      93,    93,    94,    94,    94,    94,    94,    95,    95,    96,
      96,    97,    97,    98,    98,    99,    99,    99,    99,    99,
      99,   100,   100,   101,   101,   102,   102,   103,   103,   104,
     104,   105,   105,   105,   106,   106,   107,   107,   107,   108,
     108,   109,   109,   110,   110,   110,   111,   112,   112,   113,
     113,   114,   114,   114,   114
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     2,     1,     2,     1,     2,     1,     1,     1,
       3,     4,     3,     3,     1,     1,     3,     1,     4,     1,
       1,     1,     6,     5,     1,     0,     3,     1,     2,     3,
       1,     1,     3,     1,     1,     6,     4,     6,     1,     1,
       1,     1,     3,     5,     4,     6,     4,     6,     2,     1,
       4,     2,     0,     2,     0,     2,     3,     2,     3,     2,
       2,     1,     1,     1,     1,     1,     1,     3,     1,     3,
       1,     2,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     3,     1,     1,     1,     3,     1,     1,     1,     3,
       1,     1,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     1,     4,     3,     1,     1,     4,     1,     0,     3,
       1,     1,     1,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        YY_LAC_DISCARD ("YYBACKUP");                              \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


/* Given a state stack such that *YYBOTTOM is its bottom, such that
   *YYTOP is either its top or is YYTOP_EMPTY to indicate an empty
   stack, and such that *YYCAPACITY is the maximum number of elements it
   can hold without a reallocation, make sure there is enough room to
   store YYADD more elements.  If not, allocate a new stack using
   YYSTACK_ALLOC, copy the existing elements, and adjust *YYBOTTOM,
   *YYTOP, and *YYCAPACITY to reflect the new capacity and memory
   location.  If *YYBOTTOM != YYBOTTOM_NO_FREE, then free the old stack
   using YYSTACK_FREE.  Return 0 if successful or if no reallocation is
   required.  Return YYENOMEM if memory is exhausted.  */
static int
yy_lac_stack_realloc (YYPTRDIFF_T *yycapacity, YYPTRDIFF_T yyadd,
#if YYDEBUG
                      char const *yydebug_prefix,
                      char const *yydebug_suffix,
#endif
                      yy_state_t **yybottom,
                      yy_state_t *yybottom_no_free,
                      yy_state_t **yytop, yy_state_t *yytop_empty)
{
  YYPTRDIFF_T yysize_old =
    *yytop == yytop_empty ? 0 : *yytop - *yybottom + 1;
  YYPTRDIFF_T yysize_new = yysize_old + yyadd;
  if (*yycapacity < yysize_new)
    {
      YYPTRDIFF_T yyalloc = 2 * yysize_new;
      yy_state_t *yybottom_new;
      /* Use YYMAXDEPTH for maximum stack size given that the stack
         should never need to grow larger than the main state stack
         needs to grow without LAC.  */
      if (YYMAXDEPTH < yysize_new)
        {
          YYDPRINTF ((stderr, "%smax size exceeded%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (YYMAXDEPTH < yyalloc)
        yyalloc = YYMAXDEPTH;
      yybottom_new =
        YY_CAST (yy_state_t *,
                 YYSTACK_ALLOC (YY_CAST (YYSIZE_T,
                                         yyalloc * YYSIZEOF (*yybottom_new))));
      if (!yybottom_new)
        {
          YYDPRINTF ((stderr, "%srealloc failed%s", yydebug_prefix,
                      yydebug_suffix));
          return YYENOMEM;
        }
      if (*yytop != yytop_empty)
        {
          YYCOPY (yybottom_new, *yybottom, yysize_old);
          *yytop = yybottom_new + (yysize_old - 1);
        }
      if (*yybottom != yybottom_no_free)
        YYSTACK_FREE (*yybottom);
      *yybottom = yybottom_new;
      *yycapacity = yyalloc;
    }
  return 0;
}

/* Establish the initial context for the current lookahead if no initial
   context is currently established.

   We define a context as a snapshot of the parser stacks.  We define
   the initial context for a lookahead as the context in which the
   parser initially examines that lookahead in order to select a
   syntactic action.  Thus, if the lookahead eventually proves
   syntactically unacceptable (possibly in a later context reached via a
   series of reductions), the initial context can be used to determine
   the exact set of tokens that would be syntactically acceptable in the
   lookahead's place.  Moreover, it is the context after which any
   further semantic actions would be erroneous because they would be
   determined by a syntactically unacceptable token.

   YY_LAC_ESTABLISH should be invoked when a reduction is about to be
   performed in an inconsistent state (which, for the purposes of LAC,
   includes consistent states that don't know they're consistent because
   their default reductions have been disabled).  Iff there is a
   lookahead token, it should also be invoked before reporting a syntax
   error.  This latter case is for the sake of the debugging output.

   For parse.lac=full, the implementation of YY_LAC_ESTABLISH is as
   follows.  If no initial context is currently established for the
   current lookahead, then check if that lookahead can eventually be
   shifted if syntactic actions continue from the current context.
   Report a syntax error if it cannot.  */
#define YY_LAC_ESTABLISH                                                \
do {                                                                    \
  if (!yy_lac_established)                                              \
    {                                                                   \
      YYDPRINTF ((stderr,                                               \
                  "LAC: initial context established for %s\n",          \
                  yysymbol_name (yytoken)));                            \
      yy_lac_established = 1;                                           \
      switch (yy_lac (yyesa, &yyes, &yyes_capacity, yyssp, yytoken))    \
        {                                                               \
        case YYENOMEM:                                                  \
          YYNOMEM;                                                      \
        case 1:                                                         \
          goto yyerrlab;                                                \
        }                                                               \
    }                                                                   \
} while (0)

/* Discard any previous initial lookahead context because of Event,
   which may be a lookahead change or an invalidation of the currently
   established initial context for the current lookahead.

   The most common example of a lookahead change is a shift.  An example
   of both cases is syntax error recovery.  That is, a syntax error
   occurs when the lookahead is syntactically erroneous for the
   currently established initial context, so error recovery manipulates
   the parser stacks to try to find a new initial context in which the
   current lookahead is syntactically acceptable.  If it fails to find
   such a context, it discards the lookahead.  */
#if YYDEBUG
# define YY_LAC_DISCARD(Event)                                           \
do {                                                                     \
  if (yy_lac_established)                                                \
    {                                                                    \
      YYDPRINTF ((stderr, "LAC: initial context discarded due to "       \
                  Event "\n"));                                          \
      yy_lac_established = 0;                                            \
    }                                                                    \
} while (0)
#else
# define YY_LAC_DISCARD(Event) yy_lac_established = 0
#endif

/* Given the stack whose top is *YYSSP, return 0 iff YYTOKEN can
   eventually (after perhaps some reductions) be shifted, return 1 if
   not, or return YYENOMEM if memory is exhausted.  As preconditions and
   postconditions: *YYES_CAPACITY is the allocated size of the array to
   which *YYES points, and either *YYES = YYESA or *YYES points to an
   array allocated with YYSTACK_ALLOC.  yy_lac may overwrite the
   contents of either array, alter *YYES and *YYES_CAPACITY, and free
   any old *YYES other than YYESA.  */
static int
yy_lac (yy_state_t *yyesa, yy_state_t **yyes,
        YYPTRDIFF_T *yyes_capacity, yy_state_t *yyssp, yysymbol_kind_t yytoken)
{
  yy_state_t *yyes_prev = yyssp;
  yy_state_t *yyesp = yyes_prev;
  /* Reduce until we encounter a shift and thereby accept the token.  */
  YYDPRINTF ((stderr, "LAC: checking lookahead %s:", yysymbol_name (yytoken)));
  if (yytoken == YYSYMBOL_YYUNDEF)
    {
      YYDPRINTF ((stderr, " Always Err\n"));
      return 1;
    }
  while (1)
    {
      int yyrule = yypact[+*yyesp];
      if (yypact_value_is_default (yyrule)
          || (yyrule += yytoken) < 0 || YYLAST < yyrule
          || yycheck[yyrule] != yytoken)
        {
          /* Use the default action.  */
          yyrule = yydefact[+*yyesp];
          if (yyrule == 0)
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
        }
      else
        {
          /* Use the action from yytable.  */
          yyrule = yytable[yyrule];
          if (yytable_value_is_error (yyrule))
            {
              YYDPRINTF ((stderr, " Err\n"));
              return 1;
            }
          if (0 < yyrule)
            {
              YYDPRINTF ((stderr, " S%d\n", yyrule));
              return 0;
            }
          yyrule = -yyrule;
        }
      /* By now we know we have to simulate a reduce.  */
      YYDPRINTF ((stderr, " R%d", yyrule - 1));
      {
        /* Pop the corresponding number of values from the stack.  */
        YYPTRDIFF_T yylen = yyr2[yyrule];
        /* First pop from the LAC stack as many tokens as possible.  */
        if (yyesp != yyes_prev)
          {
            YYPTRDIFF_T yysize = yyesp - *yyes + 1;
            if (yylen < yysize)
              {
                yyesp -= yylen;
                yylen = 0;
              }
            else
              {
                yyesp = yyes_prev;
                yylen -= yysize;
              }
          }
        /* Only afterwards look at the main stack.  */
        if (yylen)
          yyesp = yyes_prev -= yylen;
      }
      /* Push the resulting state of the reduction.  */
      {
        yy_state_fast_t yystate;
        {
          const int yylhs = yyr1[yyrule] - YYNTOKENS;
          const int yyi = yypgoto[yylhs] + *yyesp;
          yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyesp
                     ? yytable[yyi]
                     : yydefgoto[yylhs]);
        }
        if (yyesp == yyes_prev)
          {
            yyesp = *yyes;
            YY_IGNORE_USELESS_CAST_BEGIN
            *yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        else
          {
            if (yy_lac_stack_realloc (yyes_capacity, 1,
#if YYDEBUG
                                      " (", ")",
#endif
                                      yyes, yyesa, &yyesp, yyes_prev))
              {
                YYDPRINTF ((stderr, "\n"));
                return YYENOMEM;
              }
            YY_IGNORE_USELESS_CAST_BEGIN
            *++yyesp = YY_CAST (yy_state_t, yystate);
            YY_IGNORE_USELESS_CAST_END
          }
        YYDPRINTF ((stderr, " G%d", yystate));
      }
    }
}

/* Context of a parse error.  */
typedef struct
{
  yy_state_t *yyssp;
  yy_state_t *yyesa;
  yy_state_t **yyes;
  YYPTRDIFF_T *yyes_capacity;
  yysymbol_kind_t yytoken;
} yypcontext_t;

/* Put in YYARG at most YYARGN of the expected tokens given the
   current YYCTX, and return the number of tokens stored in YYARG.  If
   YYARG is null, return the number of expected tokens (guaranteed to
   be less than YYNTOKENS).  Return YYENOMEM on memory exhaustion.
   Return 0 if there are more than YYARGN expected tokens, yet fill
   YYARG up to YYARGN. */
static int
yypcontext_expected_tokens (const yypcontext_t *yyctx,
                            yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;

  int yyx;
  for (yyx = 0; yyx < YYNTOKENS; ++yyx)
    {
      yysymbol_kind_t yysym = YY_CAST (yysymbol_kind_t, yyx);
      if (yysym != YYSYMBOL_YYerror && yysym != YYSYMBOL_YYUNDEF)
        switch (yy_lac (yyctx->yyesa, yyctx->yyes, yyctx->yyes_capacity, yyctx->yyssp, yysym))
          {
          case YYENOMEM:
            return YYENOMEM;
          case 1:
            continue;
          default:
            if (!yyarg)
              ++yycount;
            else if (yycount == yyargn)
              return 0;
            else
              yyarg[yycount++] = yysym;
          }
    }
  if (yyarg && yycount == 0 && 0 < yyargn)
    yyarg[0] = YYSYMBOL_YYEMPTY;
  return yycount;
}




#ifndef yystrlen
# if defined __GLIBC__ && defined _STRING_H
#  define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
# else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
# endif
#endif

#ifndef yystpcpy
# if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#  define yystpcpy stpcpy
# else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
# endif
#endif

#ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;
      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
#endif


static int
yy_syntax_error_arguments (const yypcontext_t *yyctx,
                           yysymbol_kind_t yyarg[], int yyargn)
{
  /* Actual size of YYARG. */
  int yycount = 0;
  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
       In the first two cases, it might appear that the current syntax
       error should have been detected in the previous state when yy_lac
       was invoked.  However, at that time, there might have been a
       different syntax error that discarded a different initial context
       during error recovery, leaving behind the current lookahead.
  */
  if (yyctx->yytoken != YYSYMBOL_YYEMPTY)
    {
      int yyn;
      YYDPRINTF ((stderr, "Constructing syntax error message\n"));
      if (yyarg)
        yyarg[yycount] = yyctx->yytoken;
      ++yycount;
      yyn = yypcontext_expected_tokens (yyctx,
                                        yyarg ? yyarg + 1 : yyarg, yyargn - 1);
      if (yyn == YYENOMEM)
        return YYENOMEM;
      else if (yyn == 0)
        YYDPRINTF ((stderr, "No expected tokens.\n"));
      else
        yycount += yyn;
    }
  return yycount;
}

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.  In order to see if a particular token T is a
   valid looakhead, invoke yy_lac (YYESA, YYES, YYES_CAPACITY, YYSSP, T).

   Return 0 if *YYMSG was successfully written.  Return -1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return YYENOMEM if the
   required number of bytes is too large to store or if
   yy_lac returned YYENOMEM.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                const yypcontext_t *yyctx)
{
  enum { YYARGS_MAX = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  yysymbol_kind_t yyarg[YYARGS_MAX];
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* Actual size of YYARG. */
  int yycount = yy_syntax_error_arguments (yyctx, yyarg, YYARGS_MAX);
  if (yycount == YYENOMEM)
    return YYENOMEM;

  switch (yycount)
    {
#define YYCASE_(N, S)                       \
      case N:                               \
        yyformat = S;                       \
        break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
    }

  /* Compute error message size.  Don't count the "%s"s, but reserve
     room for the terminator.  */
  yysize = yystrlen (yyformat) - 2 * yycount + 1;
  {
    int yyi;
    for (yyi = 0; yyi < yycount; ++yyi)
      {
        YYPTRDIFF_T yysize1
          = yysize + yytnamerr (YY_NULLPTR, yytname[yyarg[yyi]]);
        if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
          yysize = yysize1;
        else
          return YYENOMEM;
      }
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return -1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yytname[yyarg[yyi++]]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

    yy_state_t yyesa[20];
    yy_state_t *yyes = yyesa;
    YYPTRDIFF_T yyes_capacity = 20 < YYMAXDEPTH ? 20 : YYMAXDEPTH;

  /* Whether LAC context is established.  A Boolean.  */
  int yy_lac_established = 0;
  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    {
      YY_LAC_ESTABLISH;
      goto yydefault;
    }
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      YY_LAC_ESTABLISH;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  YY_LAC_DISCARD ("shift");
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  {
    int yychar_backup = yychar;
    switch (yyn)
      {
  case 2: /* program: precomList declList  */
#line 129 "parser.y"
                        {
                           (yyval.tree) = syntaxTree = (yyvsp[0].tree);
                        }
#line 1904 "parser.tab.c"
    break;

  case 3: /* program: declList  */
#line 133 "parser.y"
                        {
                           (yyval.tree) = syntaxTree = (yyvsp[0].tree);
                        }
#line 1912 "parser.tab.c"
    break;

  case 4: /* precomList: precomList PRECOMPILER  */
#line 139 "parser.y"
                        {
                           cout << yylval.tinfo->tokenstr << endl; 
                           (yyval.tree) = NULL;
                        }
#line 1921 "parser.tab.c"
    break;

  case 5: /* precomList: PRECOMPILER  */
#line 144 "parser.y"
                        {
                           cout << yylval.tinfo->tokenstr << endl;
                           (yyval.tree) = NULL;
                        }
#line 1930 "parser.tab.c"
    break;

  case 6: /* declList: declList decl  */
#line 151 "parser.y"
                        {
                           (yyval.tree) = addSibling((yyvsp[-1].tree), (yyvsp[0].tree));
                        }
#line 1938 "parser.tab.c"
    break;

  case 7: /* declList: decl  */
#line 155 "parser.y"
                        {  (yyval.tree) = (yyvsp[0].tree); }
#line 1944 "parser.tab.c"
    break;

  case 8: /* decl: varDecl  */
#line 159 "parser.y"
                        {  (yyval.tree) = (yyvsp[0].tree);}
#line 1950 "parser.tab.c"
    break;

  case 9: /* decl: funDecl  */
#line 161 "parser.y"
                        {  (yyval.tree) = (yyvsp[0].tree);}
#line 1956 "parser.tab.c"
    break;

  case 10: /* varDecl: typeSpec varDeclList ';'  */
#line 165 "parser.y"
                        {
                           TreeNode* t = (yyvsp[-1].tree);
                           while(t)                // set type for all sliblings
                           {
                              t->type = (yyvsp[-2].expType);
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[-1].tree);
                        }
#line 1970 "parser.tab.c"
    break;

  case 11: /* scopedVarDecl: STATIC typeSpec varDeclList ';'  */
#line 176 "parser.y"
                        {
                           TreeNode* t = (yyvsp[-1].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-2].expType);           // set to typeSpec
                              t->isStatic = true;     // flag as static
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[-1].tree);
                        }
#line 1985 "parser.tab.c"
    break;

  case 12: /* scopedVarDecl: typeSpec varDeclList ';'  */
#line 187 "parser.y"
                        {
                           TreeNode* t = (yyvsp[-1].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-2].expType);
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[-1].tree);
                        }
#line 1999 "parser.tab.c"
    break;

  case 13: /* varDeclList: varDeclList ',' varDeclInit  */
#line 198 "parser.y"
                        {
                           (yyval.tree) = addSibling((yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2007 "parser.tab.c"
    break;

  case 14: /* varDeclList: varDeclInit  */
#line 202 "parser.y"
                        {
                           (yyval.tree) = (yyvsp[0].tree);
                        }
#line 2015 "parser.tab.c"
    break;

  case 15: /* varDeclInit: varDeclId  */
#line 207 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 2021 "parser.tab.c"
    break;

  case 16: /* varDeclInit: varDeclId ':' simpleExp  */
#line 209 "parser.y"
                        { (yyval.tree) = (yyvsp[-2].tree); if ((yyval.tree) != NULL) (yyval.tree)->child[0] = (yyvsp[0].tree); }
#line 2027 "parser.tab.c"
    break;

  case 17: /* varDeclId: ID  */
#line 212 "parser.y"
                        { (yyval.tree) = newDeclNode(VarK, UndefinedType, (yyvsp[0].tinfo)); }
#line 2033 "parser.tab.c"
    break;

  case 18: /* varDeclId: ID '[' NUMCONST ']'  */
#line 214 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(VarK, UndefinedType, (yyvsp[-3].tinfo));
                           (yyval.tree)->isArray = true;
                           (yyval.tree)->size = (yyvsp[-1].tinfo)->nvalue+1;
                        }
#line 2043 "parser.tab.c"
    break;

  case 19: /* typeSpec: INT  */
#line 221 "parser.y"
                        { (yyval.expType) = Integer; }
#line 2049 "parser.tab.c"
    break;

  case 20: /* typeSpec: BOOL  */
#line 223 "parser.y"
                        { (yyval.expType) = Boolean; }
#line 2055 "parser.tab.c"
    break;

  case 21: /* typeSpec: CHAR  */
#line 225 "parser.y"
                        { (yyval.expType) = Char; }
#line 2061 "parser.tab.c"
    break;

  case 22: /* funDecl: typeSpec ID '(' parms ')' stmt  */
#line 229 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(FuncK, (yyvsp[-5].expType), (yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2069 "parser.tab.c"
    break;

  case 23: /* funDecl: ID '(' parms ')' stmt  */
#line 233 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(FuncK, UndefinedType, (yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2077 "parser.tab.c"
    break;

  case 24: /* parms: parmList  */
#line 238 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 2083 "parser.tab.c"
    break;

  case 25: /* parms: %empty  */
#line 240 "parser.y"
                        { (yyval.tree) = NULL;}
#line 2089 "parser.tab.c"
    break;

  case 26: /* parmList: parmList ';' parmTypeList  */
#line 243 "parser.y"
                        {
                           (yyval.tree) = addSibling((yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2097 "parser.tab.c"
    break;

  case 27: /* parmList: parmTypeList  */
#line 247 "parser.y"
                        { (yyval.tree)  = (yyvsp[0].tree); }
#line 2103 "parser.tab.c"
    break;

  case 28: /* parmTypeList: typeSpec parmIdList  */
#line 250 "parser.y"
                        {
                           TreeNode* t = (yyvsp[0].tree);
                           while(t)
                           {
                              t->type = (yyvsp[-1].expType);           // set to typeSpec
                              //t->isStatic = true;     // flag as static
                              t = t->sibling;
                           }
                           (yyval.tree) = (yyvsp[0].tree);
                        }
#line 2118 "parser.tab.c"
    break;

  case 29: /* parmIdList: parmIdList ',' parmId  */
#line 262 "parser.y"
                        {
                           
                           (yyval.tree) = addSibling((yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2127 "parser.tab.c"
    break;

  case 30: /* parmIdList: parmId  */
#line 267 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 2133 "parser.tab.c"
    break;

  case 31: /* parmId: ID  */
#line 270 "parser.y"
                        { (yyval.tree) = newDeclNode(ParamK, UndefinedType, (yyvsp[0].tinfo)); }
#line 2139 "parser.tab.c"
    break;

  case 32: /* parmId: ID '[' ']'  */
#line 272 "parser.y"
                        {
                           (yyval.tree) = newDeclNode(ParamK, UndefinedType, (yyvsp[-2].tinfo));
                           (yyval.tree)->isArray = true;
                        }
#line 2148 "parser.tab.c"
    break;

  case 33: /* stmt: matched  */
#line 278 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree);}
#line 2154 "parser.tab.c"
    break;

  case 34: /* stmt: unmatched  */
#line 280 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 2160 "parser.tab.c"
    break;

  case 35: /* matched: IF simpleExp THEN matched ELSE matched  */
#line 284 "parser.y"
                        { (yyval.tree) = newStmtNode(IfK, (yyvsp[-5].tinfo), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2166 "parser.tab.c"
    break;

  case 36: /* matched: WHILE simpleExp DO matched  */
#line 286 "parser.y"
                        { (yyval.tree) = newStmtNode(WhileK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2172 "parser.tab.c"
    break;

  case 37: /* matched: FOR ID '=' iterRange DO matched  */
#line 288 "parser.y"
                        {
                           (yyval.tree) = makeRangeIntoFor((yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2180 "parser.tab.c"
    break;

  case 39: /* matched: compoundStmt  */
#line 293 "parser.y"
                        { (yyval.tree) = (yyvsp[0].tree); }
#line 2186 "parser.tab.c"
    break;

  case 42: /* iterRange: simpleExp TO simpleExp  */
#line 298 "parser.y"
                        { (yyval.tree) = newStmtNode(RangeK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2192 "parser.tab.c"
    break;

  case 43: /* iterRange: simpleExp TO simpleExp BY simpleExp  */
#line 300 "parser.y"
                        { (yyval.tree) = newStmtNode(RangeK, (yyvsp[-3].tinfo), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2198 "parser.tab.c"
    break;

  case 44: /* unmatched: IF simpleExp THEN stmt  */
#line 304 "parser.y"
                        { (yyval.tree) = newStmtNode(IfK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2204 "parser.tab.c"
    break;

  case 45: /* unmatched: IF simpleExp THEN matched ELSE unmatched  */
#line 306 "parser.y"
                        { (yyval.tree) = newStmtNode(IfK, (yyvsp[-5].tinfo), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2210 "parser.tab.c"
    break;

  case 46: /* unmatched: WHILE simpleExp DO unmatched  */
#line 308 "parser.y"
                        { (yyval.tree) = newStmtNode(WhileK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree)); }
#line 2216 "parser.tab.c"
    break;

  case 47: /* unmatched: FOR ID '=' iterRange DO unmatched  */
#line 310 "parser.y"
                        {
                           (yyval.tree) = makeRangeIntoFor((yyvsp[-4].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2224 "parser.tab.c"
    break;

  case 48: /* expStmt: exp ';'  */
#line 315 "parser.y"
                        { (yyval.tree) = (yyvsp[-1].tree); }
#line 2230 "parser.tab.c"
    break;

  case 49: /* expStmt: ';'  */
#line 317 "parser.y"
                        { (yyval.tree) = NULL; }
#line 2236 "parser.tab.c"
    break;

  case 50: /* compoundStmt: '{' localDecls stmtList '}'  */
#line 320 "parser.y"
                        {
                           (yyval.tree) = newStmtNode(CompoundK, (yyvsp[-3].tinfo), (yyvsp[-2].tree), (yyvsp[-1].tree));
                        }
#line 2244 "parser.tab.c"
    break;

  case 51: /* localDecls: localDecls scopedVarDecl  */
#line 325 "parser.y"
                        {
                           (yyval.tree) = addSibling((yyvsp[-1].tree), (yyvsp[0].tree));
                        }
#line 2252 "parser.tab.c"
    break;

  case 52: /* localDecls: %empty  */
#line 329 "parser.y"
                        { (yyval.tree) = NULL; }
#line 2258 "parser.tab.c"
    break;

  case 53: /* stmtList: stmtList stmt  */
#line 333 "parser.y"
                        {
                           (yyval.tree) = addSibling((yyvsp[-1].tree), (yyvsp[0].tree));
                        }
#line 2266 "parser.tab.c"
    break;

  case 54: /* stmtList: %empty  */
#line 337 "parser.y"
                        { (yyval.tree) = NULL; }
#line 2272 "parser.tab.c"
    break;

  case 55: /* returnStmt: RETURN ';'  */
#line 341 "parser.y"
                        { (yyval.tree) = newStmtNode(ReturnK, (yyvsp[-1].tinfo)); }
#line 2278 "parser.tab.c"
    break;

  case 56: /* returnStmt: RETURN exp ';'  */
#line 343 "parser.y"
                        { (yyval.tree) = newStmtNode(ReturnK, (yyvsp[-2].tinfo), (yyvsp[-1].tree)); }
#line 2284 "parser.tab.c"
    break;

  case 57: /* breakStmt: BREAK ';'  */
#line 346 "parser.y"
                        { (yyval.tree) = newStmtNode(BreakK, (yyvsp[-1].tinfo)); }
#line 2290 "parser.tab.c"
    break;

  case 58: /* exp: mutable assignop exp  */
#line 349 "parser.y"
                        {
                           (yyval.tree) = newExpNode(AssignK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2299 "parser.tab.c"
    break;

  case 59: /* exp: mutable INC  */
#line 354 "parser.y"
                        {
                           (yyval.tree) = newExpNode(AssignK, (yyvsp[0].tinfo), (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 2308 "parser.tab.c"
    break;

  case 60: /* exp: mutable DEC  */
#line 359 "parser.y"
                        {
                           (yyval.tree) = newExpNode(AssignK, (yyvsp[0].tinfo), (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 2317 "parser.tab.c"
    break;

  case 67: /* simpleExp: simpleExp OR andExp  */
#line 372 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2326 "parser.tab.c"
    break;

  case 69: /* andExp: andExp AND unaryRelExp  */
#line 379 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2335 "parser.tab.c"
    break;

  case 71: /* unaryRelExp: NOT unaryRelExp  */
#line 386 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2344 "parser.tab.c"
    break;

  case 73: /* relExp: minmaxExp relop minmaxExp  */
#line 393 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2353 "parser.tab.c"
    break;

  case 81: /* minmaxExp: minmaxExp minmaxop sumExp  */
#line 407 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2362 "parser.tab.c"
    break;

  case 85: /* sumExp: sumExp sumop mulExp  */
#line 418 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2371 "parser.tab.c"
    break;

  case 89: /* mulExp: mulExp mulop unaryExp  */
#line 429 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[-2].tree), (yyvsp[0].tree));
                           // TODO - do the thing
                        }
#line 2380 "parser.tab.c"
    break;

  case 94: /* unaryExp: unaryop unaryExp  */
#line 442 "parser.y"
                        {
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-1].tinfo), (yyvsp[0].tree));
                           if((yyval.tree)->attr.string[0] == '*')
                           {
                              (yyval.tree)->attr.name = strdup("sizeof");
                           }
                           else if((yyval.tree)->attr.string[0] == '-')
                           {
                              (yyval.tree)->attr.name = strdup("chsign");
                           }
                           // TODO - do the thing
                        }
#line 2397 "parser.tab.c"
    break;

  case 101: /* mutable: ID  */
#line 467 "parser.y"
                        {
                           (yyval.tree) = newExpNode(IdK, (yyvsp[0].tinfo));
                           // TODO - do the thing
                        }
#line 2406 "parser.tab.c"
    break;

  case 102: /* mutable: ID '[' exp ']'  */
#line 472 "parser.y"
                        {
                           TreeNode * var = newExpNode(IdK, (yyvsp[-3].tinfo));
                           (yyval.tree) = newExpNode(OpK, (yyvsp[-2].tinfo), var, (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 2416 "parser.tab.c"
    break;

  case 103: /* immutable: '(' exp ')'  */
#line 479 "parser.y"
                        { (yyval.tree) = (yyvsp[-1].tree); }
#line 2422 "parser.tab.c"
    break;

  case 106: /* call: ID '(' args ')'  */
#line 484 "parser.y"
                        {
                           (yyval.tree) = newExpNode(CallK, (yyvsp[-3].tinfo), (yyvsp[-1].tree));
                           // TODO - do the thing
                        }
#line 2431 "parser.tab.c"
    break;

  case 108: /* args: %empty  */
#line 491 "parser.y"
                        { (yyval.tree) = NULL; }
#line 2437 "parser.tab.c"
    break;

  case 109: /* argList: argList ',' exp  */
#line 494 "parser.y"
                        {
                           (yyval.tree) = addSibling((yyvsp[-2].tree), (yyvsp[0].tree));
                        }
#line 2445 "parser.tab.c"
    break;

  case 111: /* constant: NUMCONST  */
#line 500 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Integer;
                           (yyval.tree)->attr.value = (yyvsp[0].tinfo)->nvalue;
                           (yyval.tree)->lineno = (yyvsp[0].tinfo)->linenum;
                           // TODO - do the thing
                        }
#line 2457 "parser.tab.c"
    break;

  case 112: /* constant: CHARCONST  */
#line 508 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Char;
                           (yyval.tree)->attr.value = (yyvsp[0].tinfo)->cvalue;
                           (yyval.tree)->attr.cvalue = (yyvsp[0].tinfo)->cvalue;
                        }
#line 2468 "parser.tab.c"
    break;

  case 113: /* constant: STRINGCONST  */
#line 515 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Char;
                           (yyval.tree)->isArray = true;
                           (yyval.tree)->size = strlen((yyvsp[0].tinfo)->svalue)+1;
                           (yyval.tree)->attr.string = strdup((yyvsp[0].tinfo)->svalue);
                        }
#line 2480 "parser.tab.c"
    break;

  case 114: /* constant: BOOLCONST  */
#line 523 "parser.y"
                        {
                           (yyval.tree) = newExpNode(ConstantK, (yyvsp[0].tinfo));
                           (yyval.tree)->type = Boolean;
                           (yyval.tree)->attr.value = (yyvsp[0].tinfo)->nvalue;
                        }
#line 2490 "parser.tab.c"
    break;


#line 2494 "parser.tab.c"

        default: break;
      }
    if (yychar_backup != yychar)
      YY_LAC_DISCARD ("yychar change");
  }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      {
        yypcontext_t yyctx
          = {yyssp, yyesa, &yyes, &yyes_capacity, yytoken};
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        if (yychar != YYEMPTY)
          YY_LAC_ESTABLISH;
        yysyntax_error_status = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == -1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *,
                             YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (yymsg)
              {
                yysyntax_error_status
                  = yysyntax_error (&yymsg_alloc, &yymsg, &yyctx);
                yymsgp = yymsg;
              }
            else
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = YYENOMEM;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == YYENOMEM)
          YYNOMEM;
      }
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  /* If the stack popping above didn't lose the initial context for the
     current lookahead token, the shift below will for sure.  */
  YY_LAC_DISCARD ("error recovery");

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  if (yyes != yyesa)
    YYSTACK_FREE (yyes);
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
  return yyresult;
}

#line 529 "parser.y"



int main(int argc, char **argv) {
   initErrorProcessing();
   int option, index;
   char *file = NULL;
   extern FILE *yyin;
   while ((option = getopt (argc, argv, "")) != -1)
      switch (option)
      {
      default:
         ;
      }
   if ( optind == argc ) yyparse();
   for (index = optind; index < argc; index++)
   {
      yyin = fopen (argv[index], "r");
      file = argv[index];
      //initTokenStrings();
      yyparse();
      fclose (yyin);
   }

   SymbolTable* symtab;
   symtab = new SymbolTable();
   symtab->debug(false);
   int globalOffset;

   /*
   if(syntaxTree != NULL)  
      printTree(stdout, syntaxTree, true, true);

   printf("tree prited\n");
   numErrors = 1;
   */

   if(numErrors==0)
   {
      syntaxTree = semanticAnalysis(syntaxTree, symtab, globalOffset);
   }

   if(numErrors==0)
   {
      //printTree(stdout, syntaxTree, true, true);
   }
   
   if(numErrors==0)
   {
      //codegen(stdout, file, syntaxTree, symtab, globalOffset, true);
      //codegen(FILE * codeIn, char * srcFile, TreeNode * syntaxTree, SymbolTable * globalsIn, int & globalOffset, bool linenumFlagIn);
      codegen(stdout, file, syntaxTree, symtab, globalOffset, false);
   }
   
   
   fprintf(stdout, "Number of warnings: %d\n", numWarnings);
   fprintf(stdout, "Number of errors: %d\n", numErrors);
   return 0;
}

