%define parse.error verbose

%{
#include <cstdio>
#include <iostream>
#include <unistd.h>

#include "scanType.h"
#include "treeNodes.h"
#include "treeUtils.h"
#include "symbolTable.h"
#include "semantics.h"
#include "codegen.h"

#include "yyerror.h"



int numErrors=0;
int numWarnings=0;

extern int line;

extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;

static TreeNode * syntaxTree; /* stores syntax tree for later return */


using namespace std;

void printToken(TokenData myData, string tokenName, int type = 0) {
   cout << "Line: " << myData.linenum << " Type: " << tokenName;
   if(type==0)
     cout << " Token: " << myData.tokenstr;
   if(type==1)
     cout << " Token: " << myData.nvalue;
   if(type==2)
     cout << " Token: " << myData.cvalue;
   cout << endl;
}

TreeNode* makeRangeIntoFor(TokenData* mutId, TreeNode* rangeNode, TreeNode* stmtNode){
   TreeNode* forNode = newStmtNode(ForK, mutId);
   forNode->child[0] = newDeclNode(VarK, Integer, mutId);
   forNode->child[1] = rangeNode;
   forNode->child[2] = stmtNode;


   return forNode;
}

TreeNode* addSibling(TreeNode *tree, TreeNode *sib)
{
   if(tree != NULL)
   {
      TreeNode * t = tree;
      while( t->sibling != NULL )
      {
         t = t->sibling;
      }
      t->sibling = sib;
      return tree;
   }
   else
   {
      return sib;
   }
}

void printDebug(const char* st)
{
   if(1)
   {
      cout << st << endl;
   }
}

%}

%union
{
   TokenData * tinfo;
   TreeNode * tree;
   ExpType expType;

}
%token   <tinfo>  FIRSTOP
%token   <tinfo>  ARITHOPSTART
%token   <tinfo>  '+' '-' '/' '*' MIN MAX DEC INC
%token   <tinfo>  ARITHOPEND
%token   <tinfo>  RELOPSTART
%token   <tinfo>  '<' '>' LEQ GEQ EQ NEQ
%token   <tinfo>  RELOPEND
%token   <tinfo>  BOOLOPSTART
%token   <tinfo>  AND OR NOT
%token   <tinfo>  BOOLOPEND
%token   <tinfo>  '=' '?' '%' '[' ']' '{' '}' ',' ';' ':' '(' ')'
%token   <tinfo>  MULASS DIVASS ADDASS SUBASS
%token   <tinfo>  BREAK RETURN STATIC
%token   <tinfo>  IF THEN ELSE FOR TO BY DO WHILE
%token   <tinfo>  PRECOMPILER
%token   <tinfo>  LASTOP

%token   <tinfo>  CHAR INT BOOL
%token   <tinfo>  NUMCONST CHARCONST BOOLCONST STRINGCONST
%token   <tinfo>  ID
%token   <tinfo>  ERROR


%type <tinfo> assignop minmaxop mulop relop sumop unaryop

%type <tree>  program precomList declList decl varDecl funDecl
%type <tree>  varDeclList scopedVarDecl varDeclInit varDeclId
%type <tree>  parms parmList parmIdList parmId parmTypeList localDecls stmtList args argList
%type <tree>  expStmt simpleExp andExp unaryRelExp relExp minmaxExp sumExp mulExp unaryExp exp factor
%type <tree>  immutable mutable call constant matched unmatched
%type <tree>  compoundStmt returnStmt breakStmt stmt iterRange

%type <expType> typeSpec


%token   <tinfo>  LASTTERM

%%
program        :  precomList declList              // root of syntax tree
                        {
                           $$ = syntaxTree = $2;
                        }
               |  declList
                        {
                           $$ = syntaxTree = $1;
                        }
               ;

precomList     :  precomList PRECOMPILER
                        {
                           cout << yylval.tinfo->tokenstr << endl;
                           $$ = NULL;
                        }
               |  PRECOMPILER
                        {
                           cout << yylval.tinfo->tokenstr << endl;
                           $$ = NULL;
                        }
               ;

declList       :  declList decl
                        {
                           $$ = addSibling($1, $2);
                        }
               |  decl
                        {  $$ = $1; }
               ;

decl           :  varDecl
                        {  $$ = $1;}
               | funDecl
                        {  $$ = $1;}
               ;

varDecl        : typeSpec varDeclList ';'
                        {
                           TreeNode* t = $2;
                           while(t)                // set type for all sliblings
                           {
                              t->type = $1;
                              t = t->sibling;
                           }
                           $$ = $2;
                        }
               ;
scopedVarDecl  : STATIC typeSpec varDeclList ';'
                        {
                           TreeNode* t = $3;
                           while(t)
                           {
                              t->type = $2;           // set to typeSpec
                              t->isStatic = true;     // flag as static
                              t = t->sibling;
                           }
                           $$ = $3;
                        }
               | typeSpec varDeclList ';'
                        {
                           TreeNode* t = $2;
                           while(t)
                           {
                              t->type = $1;
                              t = t->sibling;
                           }
                           $$ = $2;
                        }
               ;
varDeclList    : varDeclList ',' varDeclInit          // addSibling
                        {
                           $$ = addSibling($1, $3);
                        }
               | varDeclInit
                        {
                           $$ = $1;
                        }
               ;
varDeclInit    : varDeclId
                        { $$ = $1; }
               | varDeclId ':' simpleExp
                        { $$ = $1; if ($$ != NULL) $$->child[0] = $3; }
               ;
varDeclId      : ID
                        { $$ = newDeclNode(VarK, UndefinedType, $1); }
               | ID '[' NUMCONST ']'
                        {
                           $$ = newDeclNode(VarK, UndefinedType, $1);
                           $$->isArray = true;
                           $$->size = $3->nvalue+1;
                        }
               ;
typeSpec       : INT                                        // Integer
                        { $$ = Integer; }
               | BOOL                                        // Boolean
                        { $$ = Boolean; }
               | CHAR                                        // Char
                        { $$ = Char; }
               ;

funDecl        : typeSpec ID '(' parms ')' stmt
                        {
                           $$ = newDeclNode(FuncK, $1, $2, $4, $6);
                        }
               | ID '(' parms ')' stmt                   // newDeclNode
                        {
                           $$ = newDeclNode(FuncK, UndefinedType, $1, $3, $5);
                        }
               ;
parms          : parmList
                        { $$ = $1; }
               | /* empty */                                 // NULL
                        { $$ = NULL;}
               ;
parmList       : parmList ';' parmTypeList              // addSibling
                        {
                           $$ = addSibling($1, $3);
                        }
               | parmTypeList
                        { $$  = $1; }
               ;
parmTypeList   : typeSpec parmIdList
                        {
                           TreeNode* t = $2;
                           while(t)
                           {
                              t->type = $1;           // set to typeSpec
                              //t->isStatic = true;     // flag as static
                              t = t->sibling;
                           }
                           $$ = $2;
                        }
               ;
parmIdList     : parmIdList ',' parmId                 // addSibling
                        {

                           $$ = addSibling($1, $3);
                        }
               | parmId
                        { $$ = $1; }
               ;
parmId         : ID                                          // newDeclNode
                        { $$ = newDeclNode(ParamK, UndefinedType, $1); }
               | ID '[' ']'                                   // newDeclNode
                        {
                           $$ = newDeclNode(ParamK, UndefinedType, $1);
                           $$->isArray = true;
                        }
               ;
stmt           : matched
                        { $$ = $1;}
               | unmatched
                        { $$ = $1; }
               ;

matched        : IF simpleExp THEN matched ELSE matched   // newStmtNode
                        { $$ = newStmtNode(IfK, $1, $2, $4, $6); }
               | WHILE simpleExp DO matched                  // newStmtNode
                        { $$ = newStmtNode(WhileK, $1, $2, $4); }
               | FOR ID '=' iterRange DO matched             // newStmtNode(newDeclNode)
                        {
                           $$ = makeRangeIntoFor($2, $4, $6);
                        }
               | expStmt
               | compoundStmt
                        { $$ = $1; }
               | returnStmt
               | breakStmt
               ;
iterRange      : simpleExp TO simpleExp                  // newStmtNode
                        { $$ = newStmtNode(RangeK, $2, $1, $3); }
               | simpleExp TO simpleExp BY simpleExp    // newStmtNode
                        { $$ = newStmtNode(RangeK, $2, $1, $3, $5); }

               ;
unmatched      : IF simpleExp THEN stmt                     // newStmtNode
                        { $$ = newStmtNode(IfK, $1, $2, $4); }
               | IF simpleExp THEN matched ELSE unmatched  // newStmtNode
                        { $$ = newStmtNode(IfK, $1, $2, $4, $6); }
               | WHILE simpleExp DO unmatched                // newStmtNode
                        { $$ = newStmtNode(WhileK, $1, $2, $4); }
               | FOR ID '=' iterRange DO unmatched           // newStmtNode(newDeclNode)
                        {
                           $$ = makeRangeIntoFor($2, $4, $6);
                        }
               ;
expStmt        : exp ';'
                        { $$ = $1; }
               | ';'                                        // NULL
                        { $$ = NULL; }
               ;
compoundStmt   : '{' localDecls stmtList '}'         // newStmtNode
                        {
                           $$ = newStmtNode(CompoundK, $1, $2, $3);
                        }
               ;
localDecls     : localDecls scopedVarDecl              // addSibling
                        {
                           $$ = addSibling($1, $2);
                        }
               | /* empty */                                // NULL
                        { $$ = NULL; }

               ;
stmtList       : stmtList stmt                         // addSibling
                        {
                           $$ = addSibling($1, $2);
                        }
               | /* empty */                                // NULL
                        { $$ = NULL; }

               ;
returnStmt     : RETURN ';'                                // newStmtNode
                        { $$ = newStmtNode(ReturnK, $1); }
               | RETURN exp ';'                           // newStmtNode
                        { $$ = newStmtNode(ReturnK, $1, $2); }
               ;
breakStmt      : BREAK ';'                                 // newStmtNode
                        { $$ = newStmtNode(BreakK, $1); }
               ;
exp            : mutable assignop exp                // newExpNode
                        {
                           $$ = newExpNode(AssignK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | mutable INC                              // newExpNode
                        {
                           $$ = newExpNode(AssignK, $2, $1);
                           // TODO - do the thing
                        }
               | mutable DEC                              // newExpNode
                        {
                           $$ = newExpNode(AssignK, $2, $1);
                           // TODO - do the thing
                        }
               | simpleExp
               ;
assignop       : '='
               | ADDASS
               | SUBASS
               | MULASS
               | DIVASS
               ;
simpleExp      : simpleExp OR andExp                 // newExpNode
                        {
                           $$ = newExpNode(OpK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | andExp
               ;
andExp         : andExp AND unaryRelExp              // newExpNode
                        {
                           $$ = newExpNode(OpK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | unaryRelExp
               ;
unaryRelExp    : NOT unaryRelExp                      // newExpNode
                        {
                           $$ = newExpNode(OpK, $1, $2);
                           // TODO - do the thing
                        }
               | relExp
               ;
relExp         : minmaxExp relop minmaxExp          // newExpNode
                        {
                           $$ = newExpNode(OpK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | minmaxExp
               ;
relop          : LEQ
               | '<'
               | '>'
               | GEQ
               | EQ
               | NEQ
               ;
minmaxExp      : minmaxExp minmaxop sumExp              // newExpNode
                        {
                           $$ = newExpNode(OpK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | sumExp
               ;

minmaxop       : MAX
               | MIN
               ;
sumExp         : sumExp sumop mulExp              // newExpNode
                        {
                           $$ = newExpNode(OpK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | mulExp
               ;
sumop          : '+'
               | '-'
               ;

mulExp         : mulExp mulop unaryExp           // newExpNode
                        {
                           $$ = newExpNode(OpK, $2, $1, $3);
                           // TODO - do the thing
                        }
               | unaryExp
               ;

mulop          : '*'
               | '/'
               | '%'
               ;

unaryExp       : unaryop unaryExp                   // newExpNode
                        {
                           $$ = newExpNode(OpK, $1, $2);
                           if($$->attr.string[0] == '*')
                           {
                              $$->attr.name = strdup("sizeof");
                           }
                           else if($$->attr.string[0] == '-')
                           {
                              $$->attr.name = strdup("chsign");
                           }
                           // TODO - do the thing
                        }
               | factor
               ;

unaryop        : '-'                                     // newExpNode
               | '*'                                      // newExpNode
               | '?'                                      // newExpNode
               ;

factor         : immutable
               | mutable
               ;

mutable        : ID                                       // newExpNode
                        {
                           $$ = newExpNode(IdK, $1);
                           // TODO - do the thing
                        }
               | ID '[' exp ']'                          // newExpNode
                        {
                           TreeNode * var = newExpNode(IdK, $1);
                           $$ = newExpNode(OpK, $2, var, $3);
                           // TODO - do the thing
                        }
               ;
immutable      : '(' exp ')'                            // DRBC Note: Be careful!
                        { $$ = $2; }
               | call
               | constant
               ;
call           : ID '(' args ')'                        //newExpNode
                        {
                           $$ = newExpNode(CallK, $1, $3);
                           // TODO - do the thing
                        }
               ;
args           : argList
               | /* empty */                                // NULL;
                        { $$ = NULL; }
               ;
argList        : argList ',' exp                       // addSibling
                        {
                           $$ = addSibling($1, $3);
                        }
               | exp
               ;
constant       : NUMCONST                                   // newExpNode
                        {
                           $$ = newExpNode(ConstantK, $1);
                           $$->type = Integer;
                           $$->attr.value = $1->nvalue;
                           $$->lineno = $1->linenum;
                           // TODO - do the thing
                        }
               | CHARCONST                                   // newExpNode
                        {
                           $$ = newExpNode(ConstantK, $1);
                           $$->type = Char;
                           $$->attr.value = $1->cvalue;
                           $$->attr.cvalue = $1->cvalue;
                        }
               | STRINGCONST                                 // newExpNode
                        {
                           $$ = newExpNode(ConstantK, $1);
                           $$->type = Char;
                           $$->isArray = true;
                           $$->size = strlen($1->svalue)+1;
                           $$->attr.string = strdup($1->svalue);
                        }
               | BOOLCONST                                   // newExpNode
                        {
                           $$ = newExpNode(ConstantK, $1);
                           $$->type = Boolean;
                           $$->attr.value = $1->nvalue;
                        }
               ;
%%


int main(int argc, char **argv) {
   initErrorProcessing();
   int option, index;
   char *file = NULL;
   extern FILE *yyin;
   while ((option = getopt (argc, argv, "")) != -1)
      switch (option)
      {
      default:
         ;
      }
   if ( optind == argc ) yyparse();
   for (index = optind; index < argc; index++)
   {
      yyin = fopen (argv[index], "r");
      file = argv[index];
      //initTokenStrings();
      yyparse();
      fclose (yyin);
   }

   SymbolTable* symtab;
   symtab = new SymbolTable();
   symtab->debug(false);
   int globalOffset;


   if(numErrors==0)
   {
      syntaxTree = semanticAnalysis(syntaxTree, symtab, globalOffset);
   }

   if(numErrors==0)
   {
      //printTree(stdout, syntaxTree, true, true);
   }

   if(numErrors==0)
   {
      //codegen(stdout, file, syntaxTree, symtab, globalOffset, true);
      //codegen(FILE * codeIn, char * srcFile, TreeNode * syntaxTree, SymbolTable * globalsIn, int & globalOffset, bool linenumFlagIn);
      codegen(stdout, file, syntaxTree, symtab, globalOffset, false);
   }


   fprintf(stdout, "Number of warnings: %d\n", numWarnings);
   fprintf(stdout, "Number of errors: %d\n", numErrors);
   return 0;
}

