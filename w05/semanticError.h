#ifndef SEMANTIC_ERRROR_H
#define SEMANTIC_ERROR_H


extern int line;        // line number of last token scanned in your scanner (.l)
extern char * lastToken; // last token scanned in your scanner (connect to your .l)
extern int numErrors;   // number of errors

void linkerError(const char * msg);

#endif //SEMANTIC_ERROR_H