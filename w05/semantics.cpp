#include <string.h>
#include "treeNodes.h"
#include "treeUtils.h"
#include "symbolTable.h"
#include "semantics.h"
#include "parser.tab.h"

extern int numWarnings;
extern int numErrors;

// memory offsets are GLOBAL
static int goffset; // top of global space
static int foffset; // top of local space
static int varCounter = 0;
static bool compoundStmtMakeNewScope = true;

bool insertError(TreeNode * tree, SymbolTable * symtab)
{
    if(!symtab->insert(tree->attr.name, tree))
    {
        // error condition
        return false;
    }
    return true;
}


void treeTraverseDecl(TreeNode * tree, SymbolTable * symtab)
{
    if(!tree)
    {
        return;
    }
    compoundStmtMakeNewScope = true;
    switch(tree->kind.decl)
    {
        case VarK:
            treeTraverse(tree->child[0], symtab);
            //no break intended
        case ParamK:
            if(insertError(tree, symtab))
            {
                if(symtab->depth() == 1)
                {
                    tree->varKind = Global;
                    tree->offset = goffset;
                    goffset -= tree->size;
                }
                else if(tree->isStatic)
                {
                    tree->varKind = LocalStatic;
                    tree->offset = goffset;
                    goffset -= tree->size;

                    {
                        char * newName = new char[strlen(tree->attr.name) + 10];
                        sprintf(newName, "%s-%d", tree->attr.name, ++varCounter);
                        symtab->insertGlobal(newName, tree);
                        delete[] newName;
                    }
                }
                else
                {
                    tree->varKind = Local;
                    tree->offset = foffset;
                    foffset -= tree->size;
                }
            }
            if(tree->kind.decl == ParamK)
            {
                tree->varKind = Parameter;
            }
            else if(tree->isArray)
            {
                tree->offset--;
            }
            break;
        case FuncK:
            if(insertError(tree, symtab))
            {
                if(symtab->depth() == 1)
                {
                    tree->varKind = Global;
                }
                else if(tree->isStatic)
                {
                    tree->varKind = LocalStatic;

                    {
                        char * newName = new char[strlen(tree->attr.name) + 10];
                        sprintf(newName, "%s-%d", tree->attr.name, ++varCounter);
                        symtab->insertGlobal(newName, tree);
                        delete[] newName;
                    }
                }
                else
                {
                    tree->varKind = Local;
                }
            }
            foffset = -2;
            symtab->enter(tree->attr.name);
            treeTraverse(tree->child[0], symtab);
            tree->size = foffset;
            compoundStmtMakeNewScope = false;
            treeTraverse(tree->child[1], symtab);
            tree->varKind = Global;
            symtab->leave();
            break;
    }
}
void treeTraverseStmt(TreeNode * tree, SymbolTable * symtab)
{
    if(!tree)
    {
        return;
    }
    int oldOffset;
    if(tree->kind.stmt == CompoundK)
    {
        compoundStmtMakeNewScope = true;
    }
    switch(tree->kind.stmt)
    {
        case IfK:
            treeTraverse(tree->child[0], symtab);
            treeTraverse(tree->child[1], symtab);
            treeTraverse(tree->child[2], symtab);
            break;
        case WhileK:
            treeTraverse(tree->child[0], symtab);
            treeTraverse(tree->child[1], symtab);
            break;
        case ForK:
            symtab->enter((char *) "ForStmt");
            oldOffset = foffset;
            treeTraverse(tree->child[0], symtab);
            foffset -= 2; // Make space for the for loop var
            tree->size = foffset;
            treeTraverse(tree->child[1], symtab);
            treeTraverse(tree->child[2], symtab);
            symtab->leave();
            break;
        case CompoundK:
            if(compoundStmtMakeNewScope)
            {
                symtab->enter((char *) "compoundStmt");
                oldOffset = foffset;
                treeTraverse(tree->child[0], symtab);
                tree->size = foffset;
                treeTraverse(tree->child[1], symtab);
                foffset = oldOffset;
                symtab->leave();
            }
            else
            {
                compoundStmtMakeNewScope = true;
                treeTraverse(tree->child[0], symtab);
                tree->size = foffset;
                treeTraverse(tree->child[1], symtab);
            }
            break;
        case ReturnK:
            treeTraverse(tree->child[0], symtab);
            break;
        case BreakK:
            break;
        case RangeK:
            treeTraverse(tree->child[0], symtab);
            treeTraverse(tree->child[1], symtab);
            treeTraverse(tree->child[2], symtab);
            break;
    }

}
void treeTraverseExpr(TreeNode * tree, SymbolTable * symtab)
{
    if(!tree)
    {
        return;
    }
    TreeNode * tmp;

    compoundStmtMakeNewScope = true;
    switch(tree->kind.exp)
    {
        case AssignK:
            treeTraverse(tree->child[0], symtab);
            tree->isArray = tree->child[0]->isArray;
            tree->type = tree->child[0]->type;
            treeTraverse(tree->child[1], symtab);
            break;
        case CallK:
            treeTraverse(tree->child[0], symtab);
            if((tmp = (TreeNode *) (symtab->lookup(tree->attr.name))))
            {
                tree->type = tmp->type;
                tree->isStatic = tmp->isStatic;
                tree->isArray = tmp->isArray;
                tree->size = tmp->size;
                tree->varKind = tmp->varKind;
                tree->offset = tmp->offset;
            }
            break;
        case ConstantK:
            if(tree->type == Char && tree->isArray)
            { // Deal with strings
                tree->varKind = Global;
                tree->offset = goffset - 1;
                goffset -= tree->size;
            }
            break;
        case IdK:
            if((tmp = (TreeNode *) (symtab->lookup(tree->attr.name))))
            {
                tree->type = tmp->type;
                tree->isStatic = tmp->isStatic;
                tree->isArray = tmp->isArray;
                tree->size = tmp->size;
                tree->varKind = tmp->varKind;
                tree->offset = tmp->offset;
            }
            break;
        case OpK:
        {
            treeTraverse(tree->child[0], symtab);
            //printf("\n line: %d, op: %d", tree->lineno, tree->attr.op);
            //if(tree->attr.op < 256) printf(" %c", tree->attr.op);
            if(strcmp(tree->attr.name, "chsign") == 0)
            {
                tree->type = tree->child[0]->type;
            }
            else if((tree->attr.op > BOOLOPSTART && tree->attr.op < BOOLOPEND)
                || (tree->attr.op > RELOPSTART && tree->attr.op < RELOPEND)
                || tree->attr.op == '<' || tree->attr.op == '>')

            {
                tree->type = Boolean;
            }
            else if((tree->attr.op > ARITHOPSTART && tree->attr.op < ARITHOPEND)
                || /*tree->attr.op == '[' || tree->attr.op == ']' ||*/ tree->attr.op == '*' || tree->attr.op == '?'
                || tree->attr.op == '/' || tree->attr.op == '%' || tree->attr.op == '-' || tree->attr.op == '+')
            {
                tree->type = Integer;
            }
            else if(tree->attr.op == '[')
            {
                tree->type = tree->child[0]->type;
            }
            treeTraverse(tree->child[1], symtab);
            break;
        }
    }
}


void treeTraverse(TreeNode * syntree, SymbolTable * symtab)
{
    if(!syntree)
    {
        return;
    }
    switch(syntree->nodekind)
    {
        case DeclK:
            treeTraverseDecl(syntree, symtab);
            break;
        case StmtK:
            treeTraverseStmt(syntree, symtab);
            break;
        case ExpK:
            treeTraverseExpr(syntree, symtab);
            break;
    }
    if(syntree->sibling)
    {
        treeTraverse(syntree->sibling, symtab);
    }
}

// pass in and return an annotated syntax tree
// pass in and return the symbol table
// return the offset past the globals
TreeNode * semanticAnalysis(TreeNode * syntree, SymbolTable * symtabX, int & globalOffset)
{
    syntree = loadIOLib(syntree);
    treeTraverse(syntree, symtabX);
    globalOffset = goffset;


    return syntree;
}


TreeNode * loadIOLib(TreeNode * syntree)
{
    TreeNode * input, * output, * param_output;
    TreeNode * inputb, * outputb, * param_outputb;
    TreeNode * inputc, * outputc, * param_outputc;
    TreeNode * outnl;

    input = newDeclNode(FuncK, Integer);
    input->lineno = -1; // all are -1
    input->attr.name = strdup("input"); //We named the variables well

    inputb = newDeclNode(FuncK, Boolean);
    inputb->lineno = -1; // all are -1
    inputb->attr.name = strdup("inputb"); //We named the variables well

    inputc = newDeclNode(FuncK, Char);
    inputc->lineno = -1; // all are -1
    inputc->attr.name = strdup("inputc"); //We named the variables well

    param_output = newDeclNode(ParamK, Void);
    param_output->lineno = -1; // all are -1
    param_output->attr.name = strdup("*dummy*");
    param_output->type = Integer;

    output = newDeclNode(FuncK, Void);
    output->lineno = -1; // all are -1
    output->attr.name = strdup("output");
    output->child[0] = param_output;;

    param_outputb = newDeclNode(ParamK, Void);
    param_outputb->lineno = -1; // all are -1
    param_outputb->attr.name = strdup("*dummy*");
    param_outputb->type = Boolean;

    outputb = newDeclNode(FuncK, Void);
    outputb->lineno = -1; // all are -1
    outputb->attr.name = strdup("outputb");
    outputb->child[0] = param_outputb;

    param_outputc = newDeclNode(ParamK, Void);
    param_outputc->lineno = -1; // all are -1
    param_outputc->attr.name = strdup("*dummy*");
    param_outputc->type = Char;

    outputc = newDeclNode(FuncK, Void);
    outputc->lineno = -1; // all are -1
    outputc->attr.name = strdup("outputc");
    outputc->child[0] = param_outputc;

    outnl = newDeclNode(FuncK, Void);
    outnl->lineno = -1; // all are -1
    outnl->attr.name = strdup("outnl");
    outnl->child[0] = NULL;


    // link them and prefix the tree we are interested in traversing.
    // This will put the symbols in the symbol table.
    input->sibling = output;
    output->sibling = inputb;
    inputb->sibling = outputb;
    outputb->sibling = inputc;
    inputc->sibling = outputc;
    outputc->sibling = outnl;
    outnl->sibling = syntree; // add in the tree we were given
    return input;
}