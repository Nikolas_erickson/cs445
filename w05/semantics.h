#ifndef SEMANTICS_H
#define SEMANTICS_H

#include <string.h>
#include "treeNodes.h"
#include "treeUtils.h"
#include "symbolTable.h"


bool insertError(TreeNode* tree, SymbolTable* symtab);
void treeTraverse(TreeNode* syntree, SymbolTable * symtab);
void treeTraverseDecl(TreeNode* syntree, SymbolTable * symtab);

TreeNode *semanticAnalysis(TreeNode *syntree,          // pass in and return an annotated syntax tree
                           SymbolTable *symtabX,       // pass in and return the symbol table
                           int &globalOffset            // return the offset past the globals
    );

TreeNode *loadIOLib(TreeNode *syntree);

#endif