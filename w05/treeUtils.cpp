#include "treeUtils.h"
#include <string.h>
#include <iostream>

using namespace std;

void printAllocation(TreeNode * theNode)
{
   fprintf(stdout, " [mem: %s loc: %d size: %d]", varKindToStr(theNode->varKind), theNode->offset, theNode->size);
}

char * varKindToStr(int kind)
{
   switch(kind)
   {
      case None:
         return (char *) "None";
      case Local:
         return (char *) "Local";
      case Global:
         return (char *) "Global";
      case Parameter:
         return (char *) "Parameter";
      case LocalStatic:
         return (char *) "LocalStatic";
      default:
         return (char *) "unknownVarKind";
   }
}


char expTypeBuffer[80];
char * expTypeToStr(ExpType type, bool isArray = false, bool isStatic = false)
{
   expTypeBuffer[0] = '\0';

   if(isStatic)
   {
      strcat(expTypeBuffer, (char *) "static ");
   }
   if(isArray)
   {
      strcat(expTypeBuffer, (char *) "array of ");
   }

   strcat(expTypeBuffer, (char *) "type ");

   switch(type)
   {
      case Void:
         strcat(expTypeBuffer, (char *) "void");
         break;
      case Integer:
         strcat(expTypeBuffer, (char *) "int");
         break;
      case Boolean:
         strcat(expTypeBuffer, (char *) "bool");
         break;
      case Char:
         strcat(expTypeBuffer, (char *) "char");
         break;
      case UndefinedType:
         strcat(expTypeBuffer, (char *) "undefinedType");
         break;
      default:
         strcat(expTypeBuffer, (char *) "ERROR: UNKNOWN TYPE (function expTypeToStr)");
         break;
   }
   return expTypeBuffer;
}

TreeNode * newDeclNode(DeclKind kind, ExpType type, TokenData * token, TreeNode * c0, TreeNode * c1, TreeNode * c2)
{
   TreeNode * newNode = new TreeNode;
   newNode->child[0] = c0;
   newNode->child[1] = c1;
   newNode->child[2] = c2;
   newNode->nodekind = DeclK;
   newNode->kind.decl = kind;
   newNode->attr.name = strdup("defaultName");
   if(token)
   {
      newNode->lineno = token->linenum;
      newNode->attr.name = strdup(token->svalue);
   }
   newNode->type = type;

   // for functions with undefined types, set to void type
   if(kind == FuncK && type == UndefinedType)
   {
      newNode->type = Void;
   }
   // for variables with undefined types, set to type int
   if(kind == VarK && type == UndefinedType)
   {
      newNode->type = Integer;
   }

   return newNode;
}

TreeNode * newStmtNode(StmtKind kind, TokenData * token, TreeNode * c0, TreeNode * c1, TreeNode * c2)
{
   TreeNode * newNode = new TreeNode;
   newNode->child[0] = c0;
   newNode->child[1] = c1;
   newNode->child[2] = c2;
   newNode->nodekind = StmtK;
   newNode->kind.stmt = kind;
   if(token)
   {
      newNode->lineno = token->linenum;
      newNode->attr.name = strdup(token->svalue);
   }
   return newNode;
}

TreeNode * newExpNode(ExpKind kind, TokenData * token, TreeNode * c0, TreeNode * c1, TreeNode * c2)
{
   TreeNode * newNode = new TreeNode;
   newNode->child[0] = c0;
   newNode->child[1] = c1;
   newNode->child[2] = c2;
   newNode->nodekind = ExpK;
   newNode->kind.exp = kind;
   if(token)
   {
      newNode->lineno = token->linenum;
      newNode->attr.op = token->tokenclass;
      newNode->attr.string = strdup(token->svalue);
      newNode->attr.name = strdup(token->svalue);

   }
   return newNode;
}

static void printSpaces(FILE * listing, int depth)
{
   for(int i = 0; i < depth; i++) fprintf(listing, ".   ");
}

void printTreeNode(FILE * listing, TreeNode * tree, bool showExpType, bool showAllocation)
{
   switch(tree->nodekind)
   {
      case DeclK:
      {
         // its a declaration
         char * expTypeStr = expTypeToStr(tree->type, tree->isArray, tree->isStatic);
         switch(tree->kind.decl)
         { //VarK, FuncK, ParamK
            case FuncK:
            {
               fprintf(listing, "Func: %s returns %s", tree->attr.name, expTypeStr);
               break;
            }
            case VarK:
            {
               fprintf(listing, "Var: %s of ", tree->attr.name);
               fprintf(listing, "%s", expTypeStr);
               break;
            }
            case ParamK:
            {
               fprintf(listing, "Parm: %s of %s", tree->attr.name, expTypeStr);
               break;
            }
            default:
               break;

         }
         break;
      }
      case StmtK:
      {
         // its a statement node
         switch(tree->kind.stmt)
         { ///IfK, WhileK, ForK, CompoundK, ReturnK, BreakK, RangeK
            case IfK:
            {
               showAllocation = false;
               fprintf(listing, "If");
               break;
            }
            case WhileK:
            {
               showAllocation = false;
               fprintf(listing, "While");
               break;
            }
            case ForK:
            {
               fprintf(listing, "For");
               break;
            }
            case CompoundK:
            {
               fprintf(listing, "Compound");
               break;
            }
            case ReturnK:
            {
               showAllocation = false;
               fprintf(listing, "Return");
               break;
            }
            case BreakK:
            {
               showAllocation = false;
               fprintf(listing, "Break");
               break;
            }
            case RangeK:
            {
               showAllocation = false;
               fprintf(listing, "Range");
               break;
            }
         }
         break;
      }
      case ExpK:
      {
         // its an expression node

         char * expTypeStr = expTypeToStr(tree->type, tree->isArray, tree->isStatic);
         switch(tree->kind.exp)
         { //AssignK, CallK, ConstantK, IdK, OpK
            case AssignK:
            {
               fprintf(listing, "Assign: %s", tree->attr.string);
               showAllocation = false;
               break;
            }
            case CallK:
            {
               showAllocation = false;
               fprintf(listing, "Call: %s", tree->attr.string);
               break;
            }
            case ConstantK:
            {
               fprintf(listing, "Const ");
               showAllocation = false;
               switch(tree->type)
               {
                  case Integer:
                  {
                     fprintf(listing, "%d", tree->attr.value);
                     break;
                  }
                  case Boolean:
                  {
                     if(tree->attr.value)
                     {
                        fprintf(listing, "true");
                     }
                     else
                     {
                        fprintf(listing, "false");
                     }
                     break;
                  }
                  case Char:
                  {
                     if(tree->isArray)      //if it is string
                     {
                        showAllocation = true;
                        fprintf(listing, "\"%s\"", tree->attr.string);
                     }
                     else
                     {
                        fprintf(listing, "'%c'", tree->attr.value);
                     }
                     break;
                  }
               }
               break;
            }
            case IdK:
            {
               fprintf(listing, "Id: %s", tree->attr.string);
               break;
            }
            case OpK:
            {
               showAllocation = false;
               fprintf(listing, "Op: %s", tree->attr.name);
               break;
            }
         }
         fprintf(listing, " of %s", expTypeStr);
         break;
      }
   }

   if(showAllocation)
   {
      printAllocation(tree);
   }
   fprintf(listing, " [line: %d]", tree->lineno);
}

void printTreeRec(FILE * listing, int depth, int siblingCnt, TreeNode * tree, bool showExpType, bool showAllocation)
{
   int childCnt;
   if(tree != NULL)
   {
      // print self
      printTreeNode(listing, tree, showExpType, showAllocation);
      fprintf(listing, "\n");

      // print children
      for(childCnt = 0; childCnt < MAXCHILDREN; childCnt++)
      {
         if(tree->child[childCnt])
         {
            printSpaces(listing, depth);
            fprintf(listing, "Child: %d  ", childCnt);
            printTreeRec(listing, depth + 1, 1, tree->child[childCnt], showExpType, showAllocation);
         }
      }
   }
   // print sibling
   tree = tree->sibling;
   if(tree != NULL)
   {
      if(depth)
      {
         printSpaces(listing, depth - 1);
         fprintf(listing, "Sibling: %d  ", siblingCnt);
      }
      printTreeRec(listing, depth, siblingCnt + 1, tree, showExpType, showAllocation);
   }
   fflush(listing);
}

void printTree(FILE * listing, TreeNode * tree, bool showExpType, bool showAllocation)
{
   if(tree == NULL)
   {
      printf("NULL tree\n");
      return;
   }
   printTreeRec(listing, 1, 1, tree, showExpType, showAllocation);
}


